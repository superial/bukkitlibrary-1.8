package de.superioz.sx.bukkit.test;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.exception.CommandRegisterException;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created on 17.04.2016.
 */
public class TestPlugin extends JavaPlugin {

	@Override
	public void onEnable(){
		BukkitLibrary.initFor(this);

		try{
			BukkitLibrary.registerCommand(TestCommand.class);
		}
		catch(CommandRegisterException e){
			e.printStackTrace();
		}
	}

}
