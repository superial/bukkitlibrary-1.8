package de.superioz.sx.bukkit.test;

import de.superioz.sx.bukkit.common.GameProfileBuilder;
import de.superioz.sx.bukkit.common.NickRegistry;
import de.superioz.sx.bukkit.common.command.AllowedCommandSender;
import de.superioz.sx.bukkit.common.command.Command;
import de.superioz.sx.bukkit.common.command.CommandCase;
import de.superioz.sx.bukkit.common.command.context.CommandContext;
import de.superioz.sx.bukkit.common.protocol.WrappedGameProfile;
import de.superioz.sx.bukkit.message.BukkitChat;
import de.superioz.sx.bukkit.util.BukkitUtilities;
import de.superioz.sx.bukkit.util.ChatUtil;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created on 17.04.2016.
 */
@Command(label = "test",
		commandTarget = AllowedCommandSender.PLAYER)
public class TestCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		Player player = context.getSenderAsPlayer();
		player.sendMessage(ChatColor.RED + "Usage: {set | reset | refresh | nick}");
	}

	// ===

	@Command(label = "nick",
			commandTarget = AllowedCommandSender.PLAYER)
	public void nick(final CommandContext context){
		final Player player = context.getSenderAsPlayer();
		TextComponent message = new TextComponent(ChatColor.BLUE + "Unnicked.");

		if(context.getArgumentsLength() == 0){
			NickRegistry.unickPlayer(player, BukkitUtilities.onlinePlayers());
			player.spigot().sendMessage(message);
		}
		else{
			final String name = context.getArgument(1);
			new Thread(new Runnable() {
				@Override
				public void run(){
					WrappedGameProfile gameProfile = GameProfileBuilder.get(name, name);
					if(gameProfile == null){
						BukkitChat.send("&cCouldn't nick you as " + name + "!", player);
						return;
					}
					NickRegistry.nickPlayer(player, gameProfile, true, BukkitUtilities.onlinePlayers());
					BukkitChat.send("&7Nicked as: &a" + name + "", player);
				}
			}).run();
		}
	}

}
