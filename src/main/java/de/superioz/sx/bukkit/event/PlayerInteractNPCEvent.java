package de.superioz.sx.bukkit.event;

import de.superioz.sx.bukkit.common.npc.FakeEntity;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
@Getter
public class PlayerInteractNPCEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private FakeEntity entity;
    private Action action;

    public PlayerInteractNPCEvent(Player player, FakeEntity entity, Action action){
        this.player = player;
        this.entity = entity;
        this.action = action;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
