package de.superioz.sx.bukkit.event;

import de.superioz.sx.bukkit.common.npc.FakeEntity;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created on 10.04.2016.
 */
@Getter
public class NPCUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private Player player;
	private FakeEntity entity;

	public NPCUpdateEvent(Player player, FakeEntity entity){
		this.player = player;
		this.entity = entity;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
