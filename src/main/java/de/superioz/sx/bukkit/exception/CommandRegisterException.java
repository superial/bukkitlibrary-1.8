package de.superioz.sx.bukkit.exception;

public class CommandRegisterException
  extends Exception
{
  private Reason reason;
  private Class<?> clazz;
  
  public CommandRegisterException(Reason reason, Class<?> clazz)
  {
    this.reason = reason;
    this.clazz = clazz;
  }
  
  public String getReason()
  {
    return this.reason.getReason() + " [" + this.clazz.getSimpleName() + "]";
  }
  
  public Class<?> getClazz()
  {
    return this.clazz;
  }
  
  public static enum Reason
  {
    CLASS_NOT_COMMAND_CASE("This class is not a command case!"),  COMMAND_ALREADY_EXISTS("There's a command which already exists!");
    
    private String string;
    
    private Reason(String string)
    {
      this.string = string;
    }
    
    public String getReason()
    {
      return this.string;
    }
  }
}
