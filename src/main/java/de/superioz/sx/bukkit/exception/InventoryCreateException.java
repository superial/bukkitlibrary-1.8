package de.superioz.sx.bukkit.exception;

import de.superioz.sx.bukkit.common.inventory.SuperInventory;

public class InventoryCreateException
  extends Exception
{
  private SuperInventory parent;
  private Class<?> target;
  private Reason reason;
  
  public InventoryCreateException(SuperInventory parent, Class<?> target, Reason reason)
  {
    this.parent = parent;
    this.target = target;
    this.reason = reason;
  }
  
  public SuperInventory getParent()
  {
    return this.parent;
  }
  
  public Class<?> getTarget()
  {
    return this.target;
  }
  
  public Reason getReason()
  {
    return this.reason;
  }
  
  public static enum Reason
  {
    TARGET_CLASS_NOT_ALLOWED("This class is not allowed for a inventory wrapper!");
    
    private String string;
    
    private Reason(String string)
    {
      this.string = string;
    }
    
    public String getReason()
    {
      return this.string;
    }
  }
}
