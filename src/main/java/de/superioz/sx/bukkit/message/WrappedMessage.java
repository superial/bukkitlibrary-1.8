package de.superioz.sx.bukkit.message;

import de.superioz.sx.bukkit.util.ChatUtil;
import de.superioz.sx.java.util.SimpleStringUtils;
import de.superioz.sx.java.util.SimplePair;
import lombok.Setter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 10.04.2016.
 */
public class WrappedMessage extends TextComponent {

	private Map<SimplePair<String, String>, SimplePair<ClickEvent, HoverEvent>> events = new HashMap<>();

	@Setter
	private static char surrounder = '%';

	/**
	 * Constructor of wrappedMessage
	 *
	 * @param text The message
	 */
	public WrappedMessage(TextComponent text){
		super(text);
		System.out.println("2");
	}

	public WrappedMessage(String text){
		this(new TextComponent(ChatUtil.fabulize(text)));
		System.out.println("1");
	}

	public WrappedMessage(String prefix, String text){
		super(new TextComponent(ChatUtil.fabulize(prefix + " " + text)));
	}

	/**
	 * Appends a message to the component
	 *
	 * @param text       The text
	 * @param clickEvent The clickEvent
	 * @param hoverEvent The hoverEvent
	 * @return This
	 */
	public WrappedMessage append(String text, ClickEvent clickEvent, HoverEvent hoverEvent){
		TextComponent component = new TextComponent(ChatUtil.fabulize(text));

		if(clickEvent != null){
			component.setClickEvent(new ClickEvent(clickEvent.getAction(), ChatUtil.fabulize(clickEvent.getValue())));
		}
		if(hoverEvent != null){
			component.setHoverEvent(new HoverEvent(hoverEvent.getAction(),
					TextComponent.fromLegacyText(ChatUtil.fabulize(TextComponent.toLegacyText(hoverEvent.getValue())))));
		}

		super.addExtra(component);
		return this;
	}

	public WrappedMessage append(String text, HoverEvent event){
		return this.append(text, null, event);
	}

	public WrappedMessage append(String text, ClickEvent event){
		return this.append(text, event, null);
	}

	public WrappedMessage append(String text){
		return this.append(text, null, null);
	}

	/**
	 * Replaces given texts with given events
	 *
	 * @param toReplace   The toReplace Text
	 * @param replacement The replacement text
	 * @param clickEvent  The clickEvent
	 * @param hoverEvent  The hoverEvent
	 * @return This
	 */
	public WrappedMessage replace(String toReplace, String replacement,
	                              ClickEvent clickEvent, HoverEvent hoverEvent){
		toReplace = surrounder + toReplace + surrounder;
		this.events.put(
				new SimplePair<>(toReplace, ChatUtil.colored(replacement)),
				new SimplePair<>(clickEvent, hoverEvent));
		return this;
	}

	public WrappedMessage replace(String toReplace, String replacement, ClickEvent clickEvent){
		return this.replace(toReplace, replacement, clickEvent, null);
	}

	public WrappedMessage replace(String toReplace, String replacement, HoverEvent hoverEvent){
		return this.replace(toReplace, replacement, null, hoverEvent);
	}

	/**
	 * Builds the component with the events
	 *
	 * @return The message
	 */
	public WrappedMessage applyReplacements(){
		Map<String, TextComponent> replaceComponents = new HashMap<>();

		// Set components
		for(SimplePair<String, String> d : events.keySet()){
			String toReplace = d.getType1();
			String replacement = d.getType2();
			SimplePair<ClickEvent, HoverEvent> e = events.get(d);
			ClickEvent clickEvent = e.getType1();
			HoverEvent hoverEvent = e.getType2();

			// TextComponent
			TextComponent textComponent = new TextComponent(replacement);
			if(clickEvent != null){
				textComponent.setClickEvent(new ClickEvent(clickEvent.getAction(),
						SpecialCharacter.apply(ChatUtil.colored(clickEvent.getValue()))));
			}
			if(hoverEvent != null){
				textComponent.setHoverEvent(new HoverEvent(hoverEvent.getAction(),
						TextComponent.fromLegacyText(ChatUtil.fabulize(TextComponent.toLegacyText(hoverEvent.getValue())))));
			}

			replaceComponents.put(toReplace, textComponent);
		}

		// Get parts
		List<String> toReplaces = new ArrayList<>();
		toReplaces.addAll(replaceComponents.keySet());
		String[] fullParts = SimpleStringUtils.multiSplit(getText(), toReplaces, surrounder);

		// Get all textcomponents
		List<TextComponent> components = new ArrayList<>();
		for(String s : fullParts){
			if(s == null || s.isEmpty())
				continue;

			if(replaceComponents.containsKey(s)){
				components.add(replaceComponents.get(s));
			}
			else{
				components.add(new TextComponent(s));
			}
		}

		// Create new message from textComponents
		WrappedMessage message = new WrappedMessage("");
		for(TextComponent c : components){
			message.addExtra(c);
		}

		return message;
	}

	/**
	 * Sets the prefix for the message
	 *
	 * @param text       The prefix
	 * @param clickEvent The clickEvent
	 * @param hoverEvent The hoverEvent
	 * @return The wrappedMessage
	 */
	public WrappedMessage setPrefix(String text, ClickEvent clickEvent, HoverEvent hoverEvent){
		TextComponent component = new TextComponent(ChatUtil.fabulize(text));

		if(clickEvent != null){
			component.setClickEvent(new ClickEvent(clickEvent.getAction(),
					ChatUtil.fabulize(clickEvent.getValue())));
		}
		if(hoverEvent != null){
			component.setHoverEvent(new HoverEvent(hoverEvent.getAction(),
					TextComponent.fromLegacyText(ChatUtil.fabulize(TextComponent.toLegacyText(hoverEvent.getValue())))));
		}

		component.addExtra(" ");
		component.addExtra(this);
		return new WrappedMessage(component);
	}

}
