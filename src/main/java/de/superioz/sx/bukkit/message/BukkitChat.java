package de.superioz.sx.bukkit.message;

import de.superioz.sx.bukkit.util.ChatUtil;
import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created on 07.04.2016.
 */
@Getter
public class BukkitChat {

	public static void send(String message, CommandSender... senders){
		for(CommandSender sender : senders){
			sender.sendMessage(message);
		}
	}

	/**
	 * Sends a message to given players with prefix (message)
	 *
	 * @param message The message
	 * @param prefix  The prefix
	 * @param players The players
	 */
	public static void send(String message, String prefix, Player... players){
		send(prefix + " " + message, players);
	}

	public static void send(WrappedMessage message, String prefix, Player... players){
		send(message.setPrefix(prefix, null, null), players);
	}

	/**
	 * Sends a message to given players (baseComponent)
	 *
	 * @param message The message
	 * @param players The players
	 */
	public static void send(WrappedMessage message, Player... players){
		for(Player p : players){
			p.spigot().sendMessage(message);
		}
	}


	/**
	 * Sends a message to given players (string)
	 *
	 * @param message The message
	 * @param players The players
	 */
	public static void send(String message, Player... players){
		for(Player p : players){
			p.sendMessage(ChatUtil.fabulize(message));
		}
	}

}
