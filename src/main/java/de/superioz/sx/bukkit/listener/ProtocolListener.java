package de.superioz.sx.bukkit.listener;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.npc.FakeEntity;
import de.superioz.sx.bukkit.common.npc.FakeEntityRegistry;
import de.superioz.sx.bukkit.common.protocol.PacketEvent;
import de.superioz.sx.bukkit.common.protocol.PacketHandler;
import de.superioz.sx.bukkit.common.protocol.PacketType;
import de.superioz.sx.bukkit.common.protocol.WrappedPacket;
import de.superioz.sx.bukkit.event.PlayerInteractNPCEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class ProtocolListener implements PacketHandler {


    @Override
    public void onReceive(PacketEvent event){
        WrappedPacket packet = event.getPacket();
        final Player player = event.getPlayer();

	    if(event.getType() == PacketType.Play.Client.USE_ENTITY){
            int id = (int) event.getPacket().getIntegers().read(0);

            if(id < 0) return;
            final FakeEntity entity = FakeEntityRegistry.getNPC(id);
            if(entity == null || player.getWorld() != entity.getLocation().getWorld()) return;

            if(player.isDead()) return;
            if(player.getLocation().distance(entity.getLocation()) > 8){
                return;
            }

            final Action action;
            try{
                Field field = packet.getEntityUseActions().getField(0);
                field.setAccessible(true);
                Object obj = field.get(packet.getEntityUseActions().getClassInstance());
                String actionName = (obj == null) ? "" : obj.toString();

                switch(actionName){
                    case "INTERACT":
                        action = Action.RIGHT_CLICK_AIR;
                        break;
                    case "ATTACK":
                        action = Action.LEFT_CLICK_AIR;
                        break;
                    default:
                        return;
                }
            }catch(Exception e){
                e.printStackTrace();
                return;
            }

            event.setCancelled(true);
		    new BukkitRunnable() {
			    @Override
			    public void run(){
				    PlayerInteractNPCEvent event = new PlayerInteractNPCEvent(player, entity, action);
				    BukkitLibrary.callEvent(event);
			    }
		    }.runTask(BukkitLibrary.plugin());
        }
    }

    @Override
    public void onSend(PacketEvent event){

    }
}
