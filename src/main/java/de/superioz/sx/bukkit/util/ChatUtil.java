package de.superioz.sx.bukkit.util;

import de.superioz.sx.bukkit.message.SpecialCharacter;
import org.bukkit.ChatColor;

/**
 * Class created on April in 2015
 */
public class ChatUtil {

    /**
     * Colors given string
     *
     * @param s The string
     *
     * @return The colored string
     */
    public static String colored(String s){
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    /**
     * Colors all given strings
     *
     * @param strings The strings
     *
     * @return The colored strings
     */
    public static String[] colored(String... strings){
        String[] colored = new String[strings.length];

        for(int i = 0; i < colored.length; i++){
            colored[i] = colored(strings[i]);
        }

        return colored;
    }

    /**
     * Fabulize the string
     *
     * @param s The string
     * @return The result
     */
    public static String fabulize(String s){
        return SpecialCharacter.apply(colored(s));
    }

    /**
     * Fabulize the string array
     *
     * @param strings The array
     * @return The string
     */
    public static String[] fabulize(String... strings){
        String[] colored = new String[strings.length];

        for(int i = 0; i < colored.length; i++){
            colored[i] = colored(strings[i]);
        }

        return colored;
    }

}
