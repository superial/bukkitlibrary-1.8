package de.superioz.sx.bukkit.util;

import de.superioz.sx.java.file.CustomFile;
import de.superioz.sx.java.file.SupportedFiletype;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created on 16.03.2016.
 */
@Getter
public class YamlFile extends CustomFile {

	protected FileConfiguration configuration;

	public YamlFile(String filename, String extraPath, File root){
		super(filename, extraPath, root, SupportedFiletype.YAML);
	}

	/**
	 * Saves this file
	 */
	public void save(){
		try{
			this.configuration.save(super.file);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * Get the configuration
	 *
	 * @return The configuration
	 */
	public FileConfiguration c(){
		return getConfiguration();
	}

	@Override
	public void load(String resourcePath, boolean copyDefaults, boolean create){
		super.load(resourcePath, copyDefaults, create);
		this.configuration = YamlConfiguration.loadConfiguration(super.file);
	}

	@Override
	public void load(boolean copyDefaults, boolean create){
		this.load("", copyDefaults, create);
	}

}
