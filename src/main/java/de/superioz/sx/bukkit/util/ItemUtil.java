package de.superioz.sx.bukkit.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class ItemUtil {

	private static final String HELMET_SUFFIX = "helmet";
	private static final String CHESTPLATE_SUFFIX = "chestplate";
	private static final String LEGGINGS_SUFFIX = "leggings";
	private static final String BOOTS_SUFFIX = "boots";

	/**
	 * Compares two items of equalitity
	 *
	 * @param item1 One item
	 * @param item2 Second item
	 * @return The result
	 */
	public static boolean compare(ItemStack item1, ItemStack item2){
		boolean hasMeta = item1.hasItemMeta() && item2.hasItemMeta();
		boolean sameType = item1.getType() == item2.getType();
		boolean sameDisplayname = false;
		boolean hasDisplayname = false;

		if(hasMeta){
			hasDisplayname = item1.getItemMeta().hasDisplayName()
					&& item2.getItemMeta().hasDisplayName();
		}

		if(hasDisplayname){
			sameDisplayname = item1.getItemMeta().getDisplayName().equals(item2.getItemMeta().getDisplayName());
		}

		return !(hasMeta && hasDisplayname && !sameDisplayname) && sameType;

	}

	/**
	 * Checks if the item is armor
	 *
	 * @param item The item
	 * @return The result
	 */
	public static boolean isArmor(ItemStack item){
		return isHelmet(item) || isChestplate(item) || isLeggings(item) || isBoots(item);
	}

	/**
	 * Checks if the itemstack is a helmet
	 *
	 * @param item The item
	 * @return The result
	 */
	public static boolean isHelmet(ItemStack item){
		Material type = item.getType();
		return type.name().toLowerCase().endsWith(HELMET_SUFFIX);
	}

	/**
	 * Checks if the itemstack is a chestplate
	 *
	 * @param item The item
	 * @return The result
	 */
	public static boolean isChestplate(ItemStack item){
		Material type = item.getType();
		return type.name().toLowerCase().endsWith(CHESTPLATE_SUFFIX);
	}

	/**
	 * Checks if the itemstack is a leggings
	 *
	 * @param item The item
	 * @return The result
	 */
	public static boolean isLeggings(ItemStack item){
		Material type = item.getType();
		return type.name().toLowerCase().endsWith(LEGGINGS_SUFFIX);
	}

	/**
	 * Checks if the itemstack is a boots
	 *
	 * @param item The item
	 * @return The result
	 */
	public static boolean isBoots(ItemStack item){
		Material type = item.getType();
		return type.name().toLowerCase().endsWith(BOOTS_SUFFIX);
	}

}
