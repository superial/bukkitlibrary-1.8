package de.superioz.sx.bukkit.common;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * Created on 02.05.2016.
 */
public class NametagManager {

	public static int id = 0;

	/**
	 * Set the nametag for given player
	 *
	 * @param player  The player
	 * @param prefix  The prefix
	 * @param suffix  The suffix
	 * @param viewers The viewers
	 */
	public static void setNametag(Player player, String prefix, String suffix, Player... viewers){
		for(Player p : viewers){
			Scoreboard b = p.getScoreboard();
			if(b == null){
				b = Bukkit.getScoreboardManager().getNewScoreboard();
				p.setScoreboard(b);
			}

			Team team = getTeam(b, prefix, suffix);
			String name = player.getName();
			if(!team.getEntries().contains(name)){
				team.addEntry(name);
			}
		}
	}

	/**
	 * Get the team of given scoreboard and given values
	 *
	 * @param scoreboard The scoreboard
	 * @param prefix     The prefix
	 * @param suffix     The suffix
	 * @return The team
	 */
	private static Team getTeam(Scoreboard scoreboard, String prefix, String suffix){
		Team team = null;
		for(Team t : scoreboard.getTeams()){
			if(t.getPrefix().equals(prefix) && t.getSuffix().equals(suffix)){
				team = t;
			}
		}

		if(team == null){
			team = scoreboard.registerNewTeam(++id + "-SPRL");
			team.setPrefix(prefix);
			team.setSuffix(suffix);
		}
		return team;
	}

}
