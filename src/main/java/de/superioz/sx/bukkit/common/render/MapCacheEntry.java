package de.superioz.sx.bukkit.common.render;

import lombok.Getter;

/**
 * Created on 24.04.2016.
 */
@Getter
public class MapCacheEntry {

	private String imageName;
	private String url;
	private boolean queueTask;

	public MapCacheEntry(String imageName, String url, boolean queueTask){
		this.imageName = imageName;
		this.url = url;
		this.queueTask = queueTask;
	}

}
