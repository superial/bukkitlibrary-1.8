package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.common.protocol.EnumWrappers;
import de.superioz.sx.bukkit.common.protocol.PacketType;
import de.superioz.sx.bukkit.common.protocol.ProtocolUtil;
import de.superioz.sx.bukkit.common.protocol.WrappedPacket;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Inventory of a fake human protocol and it handle the metadata for it
 * On using dont forget to update the inventory
 */
@Getter
public class EntityInventory {

	private Map<EnumWrappers.ItemSlot, ItemStack> map = new HashMap<>();

	/**
	 * Get itemstack with given slot
	 *
	 * @return The itemstack
	 */
	public ItemStack getSlot(EnumWrappers.ItemSlot slot){
		return map.get(slot);
	}

	/**
	 * Set given slot with given item
	 *
	 * @param slot The slot
	 * @param item The item
	 */
	public void setSlot(EnumWrappers.ItemSlot slot, ItemStack item){
		this.map.put(slot, item);
	}

	/**
	 * Clears the inventory
	 */
	public void clear(){
		for(EnumWrappers.ItemSlot slot : EnumWrappers.ItemSlot.values()){
			this.setSlot(slot, new ItemStack(Material.AIR));
		}
	}

	/**
	 * Create packet
	 *
	 * @param entityId The entity id
	 * @return The wrappedpacket list
	 */
	public List<WrappedPacket> createPackets(int entityId){
		List<WrappedPacket> packetList = new ArrayList<>();
		for(EnumWrappers.ItemSlot slot : EnumWrappers.ItemSlot.values()){
			ItemStack stack = this.getSlot(slot);

			WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_EQUIPMENT);
			packet.getIntegers().write(0, entityId);
			packet.getIntegers().write(1, slot.getSlot());
			packet.getItemModifier().write(0, ProtocolUtil.asNMSCopy(stack));
			packetList.add(packet);
		}
		return packetList;
	}

}
