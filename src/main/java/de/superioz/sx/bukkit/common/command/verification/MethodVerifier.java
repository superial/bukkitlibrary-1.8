package de.superioz.sx.bukkit.common.command.verification;

import de.superioz.sx.java.util.Consumer;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created on 10.04.2016.
 */
public class MethodVerifier {

	private static Map<Integer, Consumer<MethodVerifierEvent>> map = new HashMap<>();
	private static Map<UUID, List<Integer>> playerMap = new HashMap<>();
	public static final String COMMAND_NAME = "methodverification";

	/**
	 * Get a id for given player
	 *
	 * @param player The player
	 * @return The id
	 */
	private static int getID(Player player){
		int id = -1;
		int current = 1;

		while(id < 0){
			if(!has(player, current)){
				id = current;
			}
			current++;
		}
		return id;
	}

	/**
	 * Gets a consumer from given id
	 *
	 * @param id The id
	 * @return The consumer
	 */
	public static Consumer<MethodVerifierEvent> get(int id){
		if(!has(id)){
			return null;
		}
		Consumer<MethodVerifierEvent> c = map.get(id);
		map.remove(id);

		return c;
	}

	public static Consumer<MethodVerifierEvent> get(Player player, int id){
		if(!has(player, id)){
			return null;
		}
		List<Integer> ids = playerMap.get(player.getUniqueId());
		ids.remove(Integer.valueOf(id));
		playerMap.put(player.getUniqueId(), ids);

		return map.get(id);
	}

	/**
	 * Check if id exists in map
	 *
	 * @param id The id
	 * @return The result
	 */
	public static boolean has(int id){
		return map.containsKey(id);
	}

	public static boolean has(Player player, int id){
		UUID uuid = player.getUniqueId();

		if(!playerMap.containsKey(uuid)){
			return false;
		}
		List<Integer> ids = playerMap.get(uuid);

		return ids.contains(id);
	}

	/**
	 * Set a consumer to given id
	 *
	 * @param id       The id
	 * @param consumer The consumer
	 */
	public static void set(int id, Consumer<MethodVerifierEvent> consumer){
		if(!has(id)){
			map.put(id, consumer);
		}
	}

	public static void set(Player player, int id, Consumer<MethodVerifierEvent> consumer){
		if(!has(player, id)){
			map.put(id, consumer);
			List<Integer> ids;

			if(!playerMap.containsKey(player.getUniqueId())){
				ids = new ArrayList<>();
			}
			else{
				ids = playerMap.get(player.getUniqueId());
			}
			ids.add(id);
			playerMap.put(player.getUniqueId(), ids);
		}
	}

	/**
	 * Verifies a method
	 *
	 * @param player   The player
	 * @param consumer The consumer
	 * @return The id
	 */
	public static int verify(Player player, Consumer<MethodVerifierEvent> consumer){
		int id = getID(player);
		set(player, id, consumer);
		return id;
	}

	/**
	 * Method when the id is verified
	 *
	 * @param id     The id
	 * @param result The result (true | false)
	 * @param player The player
	 */
	public static void verified(int id, boolean result, Player player){
		Consumer<MethodVerifierEvent> c = get(player, id);

		if(c == null){
			return;
		}
		c.accept(new MethodVerifierEvent(result, player));
	}

	/**
	 * Get command for result and id
	 *
	 * @param id     The id
	 * @param result The result
	 * @return The command without "/"
	 */
	public static String getCommand(int id, boolean result){
		return COMMAND_NAME + " " + id + " " + (result ? "-a" : "");
	}

}
