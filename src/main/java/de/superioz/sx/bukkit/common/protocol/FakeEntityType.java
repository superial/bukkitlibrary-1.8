package de.superioz.sx.bukkit.common.protocol;

import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;
import org.bukkit.entity.EntityType;

/**
 * Created on 25.04.2016.
 */
@Getter
public enum FakeEntityType {

	OBJ_BOAT(1),
	OBJ_ITEM_STACK(2),
	OBJ_MINECART(10),
	OBJ_MINECART_STORAGE(11),
	OBJ_MINECART_POWERED(12),
	OBJ_ACTIVATED_TNT(50),
	OBJ_ENDER_CRYSTAL(51),
	OBJ_ARROW_PROJECTILE(60),
	OBJ_SNOWBALL_PROJECTILE(61),
	OBJ_EGG_PROJECTILE(62),
	OBJ_FIRE_BALL_GHAST(63),
	OPJ_FIRE_BALL_BLAZE(64),
	OBJ_THROWN_ENDERPEARL(65),
	OBJ_WITHER_SKULL(66),
	OBJ_FALLING_BLOCK(70),
	OBJ_ITEM_FRAME(71),
	OBJ_EYE_OF_ENDER(72),
	OBJ_THROWN_POTION(73),
	OBJ_FALLING_DRAGON_EGG(74),
	OBJ_THROWN_EXP_BOTTLE(75),
	OBJ_FIREWORK(76),
	OBJ_FISHING_FLOAT(90),

	// Other
	PLAYER(-1),

	// Not living
	DROPPED_ITEM(1),
	EXPERIENCE_ORB(2),
	LEASH_HITCH(8),
	PAINTING(9),
	ARROW(10),
	SNOWBALL(11),
	FIREBALL(12),
	SMALL_FIREBALL(13),
	ENDER_PEARL(14),
	ENDER_SIGNAL(15),
	THROWN_EXP_BOTTLE(17),
	ITEM_FRAME(18),
	WITHER_SKULL(19),
	PRIMED_TNT(20),
	FALLING_BLOCK(21),
	FIREWORK(22),
	ARMOR_STAND(30),
	MINECART_COMMAND(40),
	BOAT(41),
	MINECART(42),
	MINECART_CHEST(43),
	MINECART_FURNACE(44),
	MINECART_TNT(45),
	MINECART_HOPPER(46),
	MINECART_MOB_SPAWNER(47),

	// Mobs
	CREEPER(50),
	SKELETON(51),
	WITHER_SKELETON(51),
	SPIDER(52),
	GIANT(53),
	ZOMBIE(54),
	SLIME(55),
	GHAST(56),
	PIG_ZOMBIE(57),
	ENDERMAN(58),
	CAVE_SPIDER(59),
	SILVERFISH(60),
	BLAZE(61),
	MAGMA_CUBE(62),
	ENDER_DRAGON(63),
	WITHER(64),
	BAT(65),
	WITCH(66),
	ENDERMITE(67),
	GUARDIAN(68),
	PIG(90),
	SHEEP(91),
	COW(92),
	CHICKEN(93),
	SQUID(94),
	WOLF(95),
	MUSHROOM_COW(96),
	SNOWMAN(97),
	OCELOT(98),
	IRON_GOLEM(99),
	HORSE(100),
	RABBIT(101),
	VILLAGER(120),
	ENDER_CRYSTAL(200);


	private int identifier;
	private boolean living;

	/**
	 * Constructor for the protocol protocol mobType
	 */
	FakeEntityType(int identifier){
		this.identifier = identifier;
		this.living = (identifier >= 50 && identifier < 200
				&& !name().toLowerCase().startsWith("obj_"));
	}

	/**
	 * Gets entity mobType from given id
	 *
	 * @param identifier The id
	 * @return The entity mobType
	 */
	public static FakeEntityType fromEID(int identifier){
		for(FakeEntityType t : values()){
			if(t.getIdentifier() == identifier)
				return t;
		}
		throw new RuntimeException("Couldn't get entity mobType from id='" + identifier + "'.");
	}

	/**
	 * Gets entity mobType from given index
	 *
	 * @param index The index
	 * @return The entity mobType
	 */
	public static FakeEntityType fromIndex(int index){
		for(FakeEntityType t : values()){
			if(t.ordinal() == index)
				return t;
		}
		throw new RuntimeException("Couldn't get entity mobType from index='" + index + "'.");
	}

	/**
	 * Get the name
	 *
	 * @return The name
	 */
	public String getName(){
		return SimpleStringUtils.upperFirstLetterSpaced(name().toLowerCase(), "_").replace(" ", "");
	}

	/**
	 * Get the entity mobType from this mobType
	 *
	 * @return The mobType
	 */
	public EntityType toEntityType(){
		if(getIdentifier() < 0){
			return EntityType.PLAYER;
		}
		return EntityType.fromId(getIdentifier());
	}

}
