package de.superioz.sx.bukkit.common.protocol;

import lombok.Getter;

/**
 * Created on 02.04.2016.
 */
public class EnumWrappers {

	@Getter
	public enum ScoreboardTeamAction {

		CREATE_TEAM(0),
		REMOVE_TEAM(1),
		UPDATE_TEAM(2),
		ADD_PLAYERS(3),
		REMOVE_PLAYERS(4);

		private int id;

		ScoreboardTeamAction(int i){
			this.id = i;
		}
	}

	@Getter
	public enum PlayerDifficulty {

		PEACEFUL(0, "options.difficulty.peaceful"),
		EASY(1, "options.difficulty.easy"),
		NORMAL(2, "options.difficulty.normal"),
		HARD(3, "options.difficulty.hard");

		private int id;
		private String option;

		PlayerDifficulty(int i, String s){
			this.id = i;
			this.option = s;
		}
	}

	public enum PlayerInfoAction {

		ADD_PLAYER,
		UPDATE_GAME_MODE,
		UPDATE_LATENCY,
		UPDATE_DISPLAY_NAME,
		REMOVE_PLAYER

	}

	@Getter
	public enum PlayerAnimation {

		SWING_ARM(0),
		TAKE_DAMAGE(1),
		LEAVE_BED(2),
		EAT_FOOD(3),
		CRITICAL_EFFECT(4),
		MAGIC_CRITICAL_EFFECT(5);

		private int id;

		PlayerAnimation(int i){
			this.id = i;
		}
	}

	public enum NativeGameMode {

		NOT_SET,
		SURVIVAL,
		CREATIVE,
		ADVENTURE,
		SPECTATOR,

	}

	@Getter
	public enum ItemSlot {

		HAND(0),
		FEET(1),
		LEGS(2),
		CHEST(3),
		HEAD(4);

		private int slot;

		ItemSlot(int i){
			this.slot = i;
		}

	}

	public enum TitleAction {

		TITLE,
		SUBTITLE,
		TIMES,
		CLEAR,
		RESET

	}

	public enum EntityUseAction {

		INTERACT,
		ATTACK,
		INTERACT_AT

	}

	public enum WorldBorderAction {

		SET_SIZE,
		LERP_SIZE,
		SET_CENTER,
		INITIALIZE,
		SET_WARNING_TIME,
		SET_WARNING_BLOCKS

	}

}
