package de.superioz.sx.bukkit.common.protocol;

import de.superioz.sx.java.util.ReflectionUtils;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 24.04.2016.
 */
@Getter
public class WrappedTagCompounder {

	private Object handle;
	private Class<?> handleClass;
	private Object nmsItemStack;

	private static final String SET_INT = "setInt";
	private static final String SET_STRING = "setString";
	private static final String SET_BOOL = "setBoolean";

	private static final String GET_INT = "getInt";
	private static final String GET_STRING = "getString";
	private static final String GET_BOOL = "getBoolean";

	private static final String HAS_KEY = "hasKey";
	private static final String GET_TAG = "getTag";
	private static final String SET_TAG = "setTag";

	public WrappedTagCompounder(ItemStack itemStack){
		this.nmsItemStack = ProtocolUtil.asNMSCopy(itemStack);

		this.handle = ReflectionUtils.invokeMethod(getNmsItemStack(), GET_TAG);
		if(this.handle == null){
			this.handle = ReflectionUtils.instantiateObject(ProtocolUtil.NBT_TAG_COMPOUND);
		}
		this.handleClass = getHandle().getClass();
	}

	/**
	 * Turns values into bukkit stack
	 *
	 * @return The itemstack
	 */
	public ItemStack toBukkitStack(){
		ReflectionUtils.fastMethodInvoking(getNmsItemStack(), SET_TAG,
				new Class[]{getHandleClass()}, getHandle());
		return ProtocolUtil.fromNMSCopy(getNmsItemStack());
	}

	/**
	 * Gets an int to given index
	 *
	 * @param index The index
	 */
	public int getInt(String index){
		return (int) ReflectionUtils.fastMethodInvoking(getHandle(), GET_INT,
				new Class[]{String.class}, index);
	}

	/**
	 * Gets a string to given index
	 */
	public String getString(String index){
		return (String) ReflectionUtils.fastMethodInvoking(getHandle(), GET_STRING,
				new Class[]{String.class}, index);
	}

	/**
	 * Gets a boolean to given index
	 *
	 * @param index The index
	 */
	public boolean getBoolean(String index){
		return (boolean) ReflectionUtils.fastMethodInvoking(getHandle(), GET_BOOL,
				new Class[]{String.class}, index);
	}

	/**
	 * Checks if compound has key
	 *
	 * @param index The index
	 * @return The result
	 */
	public boolean hasKey(String index){
		return (boolean) ReflectionUtils.fastMethodInvoking(getHandle(), HAS_KEY,
				new Class[]{String.class}, index);
	}

	/**
	 * Sets an int to given index
	 *
	 * @param index The index
	 * @param value The value
	 */
	public void setInt(String index, int value){
		ReflectionUtils.fastMethodInvoking(getHandle(), SET_INT,
				new Class[]{String.class, int.class}, index, value);
	}

	/**
	 * Sets a string to given index
	 *
	 * @param index The index
	 * @param value The value
	 */
	public void setString(String index, String value){
		ReflectionUtils.fastMethodInvoking(getHandle(), SET_STRING,
				new Class[]{String.class, String.class}, index, value);
	}

	/**
	 * Sets a boolean to given index
	 *
	 * @param index The index
	 * @param value The value
	 */
	public void setBoolean(String index, boolean value){
		ReflectionUtils.fastMethodInvoking(getHandle(), SET_BOOL,
				new Class[]{String.class, boolean.class}, index, value);
	}

}
