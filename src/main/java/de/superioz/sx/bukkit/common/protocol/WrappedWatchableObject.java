package de.superioz.sx.bukkit.common.protocol;

import de.superioz.sx.java.util.ReflectionUtils;
import lombok.Getter;

/**
 * Created on 01.04.2016.
 */
@Getter
public class WrappedWatchableObject extends AbstractWrapper {

	private int typeId;
	private int index;
	private Object value;

	/**
	 * Constructor for watchable object of datawatcher
	 *
	 * @param index The object
	 * @param value The value
	 */
	public WrappedWatchableObject(int index, Object value){
		this.index = index;
		this.value = value;
		this.typeId = WrappedDataWatcher.getClassIndex(value.getClass());

		super.initialise(ProtocolUtil.WATCHABLE_OBJECT.getSimpleName(),
				new Class<?>[]{int.class, int.class, Object.class}, typeId, index, value);
	}

	/**
	 * Constructor for handle takeover
	 *
	 * @param handle The handle
	 */
	public WrappedWatchableObject(Object handle){
		super.inherit(handle, handle.getClass());

		this.typeId = (int) ReflectionUtils.getField(getHandleClass(), getHandle(), "a");
		this.index = (int) ReflectionUtils.getField(getHandleClass(), getHandle(), "b");
		this.value = ReflectionUtils.getField(getHandleClass(), getHandle(), "c");
	}

	/**
	 * Sets the object
	 *
	 * @param object The object
	 */
	public void set(Object object){
		ReflectionUtils.fastMethodInvoking(getHandle(), "a", new Class<?>[]{Object.class}, object);
	}

}
