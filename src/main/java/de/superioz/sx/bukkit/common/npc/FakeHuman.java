package de.superioz.sx.bukkit.common.npc;

import com.mojang.authlib.properties.Property;
import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.GameProfileBuilder;
import de.superioz.sx.bukkit.common.protocol.*;
import de.superioz.sx.bukkit.util.LocationUtil;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

/**
 * Created on 01.04.2016.
 */
@Getter
public class FakeHuman extends FakeEntity {

	protected String skinName;
	protected UUID uniqueId;
	protected WrappedGameProfile gameProfile;
	protected Collection<Property> skin;

	protected Thread fetchingThread;

	private static final String TEXTURE_PROPERTY = "textures";

	/**
	 * Default constructor for npc human
	 *
	 * @param location    The location where to spawn first
	 * @param displayName The displayname
	 */
	public FakeHuman(Location location, String displayName, final String skinName){
		super(FakeEntityType.PLAYER, location, displayName, true);
		this.skinName = skinName;
		this.fetchProfile(true);
		this.setSkinLayers(true);
	}

	public FakeHuman(Location location, String displayName){
		this(location, displayName, displayName);
	}

	/**
	 * Fetches the profile
	 */
	private void fetchProfile(final boolean override){
		this.fetchingThread = new Thread(new Runnable() {
			@Override
			public void run(){
				WrappedGameProfile gp = GameProfileBuilder.get(getSkinName(), getDisplayName());
				if(override){
					gameProfile = gp;
				}
				else{
					assert gp != null;
					gameProfile.getProperties().removeAll(TEXTURE_PROPERTY);
					gameProfile.getProperties().putAll(TEXTURE_PROPERTY, gp.getProperties().get(TEXTURE_PROPERTY));
				}
				uniqueId = gameProfile.getUniqueId();
				skin = gameProfile.getProperties().get(TEXTURE_PROPERTY);
			}
		});
		this.fetchingThread.start();
	}

	/**
	 * Activate/Deactivate the skin layers
	 *
	 * @param flag The flag
	 */
	public void setSkinLayers(boolean flag){
		this.getDataWatcher().set(10, (byte) (flag ? 127 : 0));
	}

	/**
	 * Checks if this player has skin layers
	 *
	 * @return The result
	 */
	public boolean hasSkinLayers(){
		return getDataWatcher().getByte(10) == (byte) 127;
	}

	/**
	 * Set the skin
	 *
	 * @param name The name
	 */
	public void setSkin(String name){
		this.skinName = name;
		this.fetchProfile(false);
	}

	/**
	 * Set the name
	 *
	 * @param name The name
	 */
	public void setName(String name){
		super.setDisplayName(name);
		this.getGameProfile().setName(name);
	}

	/**
	 * Do a player animation
	 *
	 * @param animation The animation
	 */
	public void doAnimation(EnumWrappers.PlayerAnimation animation){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ANIMATION);

		packet.getIntegers().write(0, getEntityId());
		packet.getIntegers().write(1, animation.getId());

		packet.send(getViewerAsPlayer());
	}

	/**
	 * Add this fake player to tablist
	 */
	public void addToTablist(Player... players){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);

		packet.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
		packet.getPlayerInfoDataLists().write(0, Collections.singletonList(
				new PlayerInfoData(EnumWrappers.NativeGameMode.NOT_SET, getGameProfile(),
						0, new WrappedChatComponent(getDisplayName()))));

		packet.send(players);
	}

	/**
	 * Remove this fakeplayer from tablist
	 */
	public void removeFromTablist(){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);

		packet.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
		packet.getPlayerInfoDataLists().write(0, Collections.singletonList(new PlayerInfoData(EnumWrappers.NativeGameMode.NOT_SET,
				getGameProfile(), 0, new WrappedChatComponent(getDisplayName()))));
		packet.send(getViewerAsPlayer());
	}

	@Override
	public void spawn(Player... players){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.NAMED_ENTITY_SPAWN);

		// First add player to tablist
		// Wait for finishing thread
		try{
			this.fetchingThread.join();
			this.addToTablist(players);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}

		packet.getIntegers().write(0, getEntityId());
		packet.getIntegers().write(1, LocationUtil.toFixedPoint(getLocation().getX()));
		packet.getIntegers().write(2, LocationUtil.toFixedPoint(getLocation().getY()));
		packet.getIntegers().write(3, LocationUtil.toFixedPoint(getLocation().getZ()));
		packet.getIntegers().write(4, 0);

		packet.getBytes().write(0, (byte) LocationUtil.toAngle(getLocation().getYaw()));
		packet.getBytes().write(1, (byte) LocationUtil.toAngle(getLocation().getPitch()));

		packet.getUniqueIdentifier().write(0, getGameProfile().getUniqueId());
		packet.getDataWatcherModifier().write(0, getDataWatcher());
		packet.send(players);

		// Other
		this.updateInventory();

		// Unload from tablist
		new BukkitRunnable() {
			@Override
			public void run(){
				removeFromTablist();
			}
		}.runTaskLater(BukkitLibrary.plugin(), 25L);

		// Set viewer
		super.setViewers(true, players);
		super.register();
	}

	@Getter
	public static class Profile {

		protected String skinName;
		protected String capeUrl;

		public Profile(String capeUrl, String skinName){
			this.capeUrl = capeUrl;
			this.skinName = skinName;
		}

	}

}
