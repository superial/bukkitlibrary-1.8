package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.common.protocol.FakeEntityType;
import de.superioz.sx.bukkit.common.protocol.PacketType;
import de.superioz.sx.bukkit.common.protocol.ProtocolUtil;
import de.superioz.sx.bukkit.common.protocol.WrappedPacket;
import de.superioz.sx.bukkit.util.LocationUtil;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 24.04.2016.
 */
@Getter
public class FakeObject extends FakeEntity {

	private int objectData = 0;

	public FakeObject(FakeEntityType type, Location location, String displayName){
		super(type, location, displayName);
	}

	/**
	 * Set the itemstack if it's an itemstack
	 *
	 * @param stack The stack
	 */
	public void setItemStack(ItemStack stack){
		if(getType() == FakeEntityType.OBJ_ITEM_STACK){
			getDataWatcher().set(10, ProtocolUtil.asNMSCopy(stack));
		}
	}

	/**
	 * Set the block of the falling block
	 *
	 * @param stack The stack
	 */
	public void setBlock(ItemStack stack){
		if(getType() == FakeEntityType.OBJ_FALLING_BLOCK){
			int id = stack.getData().getItemTypeId();
			byte data = stack.getData().getData();

			this.setObjectData((id | (data << 0xC)));
		}
	}

	/**
	 * Set object data for this entity
	 *
	 * @param data The data as int
	 */
	public void setObjectData(int data){
		this.objectData = data;
	}

	@Override
	public void spawn(Player... players){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.SPAWN_ENTITY);

		packet.getIntegers().write(0, getEntityId());
		packet.getIntegers().write(1, LocationUtil.toFixedPoint(getLocation().getX()));
		packet.getIntegers().write(2, LocationUtil.toFixedPoint(getLocation().getY() + 0.001D));
		packet.getIntegers().write(3, LocationUtil.toFixedPoint(getLocation().getZ()));
		packet.getIntegers().write(7, (int) LocationUtil.toAngle(getLocation().getPitch()));
		packet.getIntegers().write(8, (int) LocationUtil.toAngle(getLocation().getYaw()));
		packet.getIntegers().write(9, getType().getIdentifier());
		packet.getIntegers().write(10, getObjectData());

		packet.send(players);

		// Set viewer
		super.setViewers(true, players);
		super.register();
	}

}
