package de.superioz.sx.bukkit.common.protocol;

import de.superioz.sx.java.util.ReflectionUtils;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created on 30.03.2016.
 */
public class WrappedDataWatcher extends AbstractWrapper {

	private static Method ADD;
	private static Method SET;
	private static Field MAP;
	private static Field ID_MAP;

	private static Object handle;

	public WrappedDataWatcher(Object ob){
		super.inherit(ob, ob.getClass());
		this.init(ob);
	}

	public WrappedDataWatcher(){
		super.initialise(ProtocolUtil.DATA_WATCHER.getSimpleName(), new Class<?>[]{ProtocolUtil.ENTITY}, (Object) null);
		this.init(getHandle());
	}

	/**
	 * Initialises this class
	 */
	public void init(Object ha){
		handle = ha;

		ADD = ReflectionUtils.getMethod(getHandleClass(), "a", int.class, Object.class);
		MAP = ReflectionUtils.getField(getHandleClass(), "d");
		ID_MAP = ReflectionUtils.getField(getHandleClass(), "c");
	}

	/**
	 * Set an object
	 *
	 * @param i      The index
	 * @param object The object
	 */
	public void set(int i, Object object){
		if(contains(i))
			this.getWrappedObject(i).set(object);
		else{
			try{
				ADD.invoke(getHandle(), i, object);
			}
			catch(IllegalAccessException | InvocationTargetException e){
				e.printStackTrace();
			}
		}
	}

	public void set(EntityMeta meta, Object object){
		this.set(meta.getIndex(), object);
	}

	/**
	 * Get the object with index
	 *
	 * @param i The index
	 * @return The object
	 */
	public Object get(int i){
		Object object = getWatchableObjects().get(i);
		WrappedWatchableObject watchableObject = new WrappedWatchableObject(object);

		return watchableObject.getValue();
	}

	public Object get(EntityMeta meta){
		return this.get(meta.getIndex());
	}

	/**
	 * Get byte from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public byte getByte(int index){
		return (byte) get(index);
	}

	public byte getByte(EntityMeta meta){
		return (byte) get(meta);
	}

	/**
	 * Get short from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public short getShort(int index){
		return (short) get(index);
	}

	public short getShort(EntityMeta index){
		return (short) get(index);
	}

	/**
	 * Get int from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public int getInt(int index){
		return (int) get(index);
	}

	public int getInt(EntityMeta index){
		return (int) get(index);
	}

	/**
	 * Get float from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public float getFloat(int index){
		return (float) get(index);
	}

	public float getFloat(EntityMeta index){
		return (float) get(index);
	}

	/**
	 * Get string from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public String getString(int index){
		return (String) get(index);
	}

	public String getString(EntityMeta index){
		return (String) get(index);
	}

	/**
	 * Get itemstack from given index
	 *
	 * @param index The index
	 * @return The result
	 */
	public ItemStack getItemStack(int index){
		return ProtocolUtil.fromNMSCopy(get(index));
	}

	public ItemStack getItemStack(EntityMeta index){
		return ProtocolUtil.fromNMSCopy(get(index));
	}

	/**
	 * Get the watchable object with given index
	 *
	 * @param i The index
	 * @return The object
	 */
	public WrappedWatchableObject getWrappedObject(int i){
		Object object = getWatchableObjects().get(i);
		return new WrappedWatchableObject(object);
	}

	public WrappedWatchableObject getWrappedObject(EntityMeta meta){
		return this.getWrappedObject(meta.getIndex());
	}

	/**
	 * Check if the datawatcher contains given index
	 *
	 * @param i The index
	 * @return The result
	 */
	public boolean contains(int i){
		return getWatchableObjects().containsKey(i);
	}

	public boolean contains(EntityMeta meta){
		return contains(meta.getIndex());
	}

	/**
	 * Get the watchable objects
	 *
	 * @return The watchable objects
	 */
	public Map<Integer, Object> getWatchableObjects(){
		try{
			return (Map<Integer, Object>) MAP.get(getHandle());
		}
		catch(IllegalAccessException e){
			e.printStackTrace();
		}
		return new HashMap<>();
	}

	/**
	 * Get list of watchable objects
	 *
	 * @return The list
	 */
	public List<WrappedWatchableObject> getWatchableObjectList(){
		List<WrappedWatchableObject> list = new ArrayList<>();
		for(Object o : getWatchableObjects().values()){
			list.add(new WrappedWatchableObject(o));
		}
		return list;
	}

	/**
	 * Get the class indexes map
	 *
	 * @return The map
	 */
	public static Map<Class<?>, Integer> getClassIdentifier(){
		try{
			return (Map<Class<?>, Integer>) ID_MAP.get(handle);
		}
		catch(IllegalAccessException e){
			e.printStackTrace();
		}
		return new HashMap<>();
	}

	/**
	 * Get the class index of given
	 *
	 * @param c The class
	 * @return The identifier
	 */
	public static Integer getClassIndex(Class<?> c){
		return getClassIdentifier().get(c);
	}

}
