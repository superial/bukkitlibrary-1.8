package de.superioz.sx.bukkit.common.protocol;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.PropertyMap;
import de.superioz.sx.java.util.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.UUID;

/**
 * Created on 25.04.2016.
 */
@Getter
public class WrappedGameProfile {

	private GameProfile handle;
	private Class<?> handleClass;

	private static Field NAME_FIELD;
	private static Field UUID_FIELD;
	private static Field LEGACY_FIELD;
	private static Field PROPERTY_MAP;

	@Setter
	private String name;
	@Setter
	private UUID uniqueId;
	@Setter
	private boolean legacy;
	@Setter
	private PropertyMap properties;

	public WrappedGameProfile(GameProfile handle){
		this.handle = handle;
		this.handleClass = handle.getClass();

		NAME_FIELD = ReflectionUtils.getField(getHandleClass(), "name");
		UUID_FIELD = ReflectionUtils.getField(getHandleClass(), "id");
		LEGACY_FIELD = ReflectionUtils.getField(getHandleClass(), "legacy");
		PROPERTY_MAP = ReflectionUtils.getField(getHandleClass(), "properties");

		try{
			this.name = (String) NAME_FIELD.get(getHandle());
			this.uniqueId = (UUID) UUID_FIELD.get(getHandle());
			this.legacy = LEGACY_FIELD.getBoolean(getHandle());
			this.properties = (PropertyMap) PROPERTY_MAP.get(getHandle());
		}
		catch(IllegalAccessException e){
			e.printStackTrace();
		}
	}

	/**
	 * Get profile from given player
	 *
	 * @param player The player
	 * @return The game profile
	 */
	public static WrappedGameProfile from(Player player){
		Object object = ProtocolUtil.getHandle(player);
		EntityPlayer player1 = (EntityPlayer) object;
		GameProfile profile = player1.getProfile();

		return new WrappedGameProfile(profile);
	}

	/**
	 * Insert the profile into the player
	 *
	 * @param player The player
	 */
	public void deepInsert(Player player){
		Object playerHandle = ProtocolUtil.getHandle(player);

		Field f = ReflectionUtils.getField(playerHandle.getClass().getSuperclass(), "bH");
		Field modifiers = ReflectionUtils.getField(Field.class, "modifiers");
		try{
			modifiers.setInt(f, modifiers.getModifiers() & ~Modifier.FINAL);
			f.set(playerHandle, getHandle());
		}
		catch(IllegalAccessException e){
			e.printStackTrace();
		}
	}

}
