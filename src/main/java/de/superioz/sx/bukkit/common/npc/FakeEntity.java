package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.common.protocol.*;
import de.superioz.sx.bukkit.common.protocol.WrappedDataWatcher;
import de.superioz.sx.bukkit.util.ChatUtil;
import de.superioz.sx.bukkit.util.LocationUtil;
import de.superioz.sx.java.util.BitMaskUtil;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.*;

/**
 * Created on 01.04.2016.
 */
@Getter
public abstract class FakeEntity {

	protected List<UUID> viewers;
	protected int entityId;
	protected Location location;
	protected WrappedDataWatcher dataWatcher;
	protected EntityInventory inventory;
	protected FakeEntityType type;
	protected List<FakeEntity> attached;

	protected boolean fixedAim;
	protected String displayName;

	/**
	 * Constructor for simply npc
	 *
	 * @param location    The default location
	 * @param displayName The display name
	 */
	public FakeEntity(FakeEntityType mobType, Location location, String displayName, boolean fixedAim){
		this.location = location;
		this.attached = new ArrayList<>();
		this.viewers = new ArrayList<>();
		this.entityId = UniqueIdManager.reserveNew();
		this.fixedAim = fixedAim;
		this.displayName = displayName;
		this.type = mobType;
		this.inventory = new EntityInventory();

		this.dataWatcher = new WrappedDataWatcher();
		this.setDisplayName(displayName);
		FakeEntityHandler.setDefaultWatchableObjects(getDataWatcher(), getType());
	}

	public FakeEntity(FakeEntityType mobType, Location location, String displayName){
		this(mobType, location, displayName, false);
	}

	/**
	 * Set the entity crouching
	 *
	 * @param f The flag
	 */
	public void setCrouching(boolean f){
		EntityMetaValue value = EntityMetaValue.ENTITY_CROUCHED;
		if(f) this.addState(value);
		else this.removeState(value);
	}

	/**
	 * Set the entity visible/invisible
	 *
	 * @param f The flag
	 */
	public void setInvisible(boolean f){
		EntityMetaValue value = EntityMetaValue.ENTITY_INVISIBLE;
		if(f) this.addState(value);
		else this.removeState(value);
	}

	/**
	 * Set the entity on fire
	 *
	 * @param f The flag
	 */
	public void setOnFire(boolean f){
		EntityMetaValue value = EntityMetaValue.ENTITY_ON_FIRE;
		if(f) this.addState(value);
		else this.removeState(value);
	}

	/**
	 * Get current entity state
	 *
	 * @return The state as byte
	 */
	public byte getState(){
		return dataWatcher.getByte(EntityMeta.ENTITY_STATE);
	}

	/**
	 * Adds a state to the entity
	 *
	 * @param value The flag
	 */
	public void addState(EntityMetaValue value){
		if(!hasState(value))
			this.dataWatcher.set(EntityMeta.ENTITY_STATE, BitMaskUtil.add(getState(), value.getBitMask()));
	}

	/**
	 * Removes a state
	 *
	 * @param value The flag
	 */
	public void removeState(EntityMetaValue value){
		if(hasState(value))
			this.dataWatcher.set(EntityMeta.ENTITY_STATE, BitMaskUtil.remove(getState(), value.getBitMask()));
	}

	/**
	 * Checks if this entity has given state
	 *
	 * @param value The state
	 * @return The result
	 */
	public boolean hasState(EntityMetaValue value){
		return BitMaskUtil.contains(getState(), value.getBitMask());
	}

	/**
	 * Set the air level
	 *
	 * @param level The level as int
	 */
	public void setAirLevel(int level){
		dataWatcher.set(EntityMeta.ENTITY_IN_AIR, level);
	}

	/**
	 * Get the air level
	 *
	 * @return The level as int
	 */
	public int getAirLevel(){
		return (int) dataWatcher.get(EntityMeta.ENTITY_IN_AIR);
	}

	/**
	 * Set if the displayname should be visible
	 *
	 * @param b The boolean
	 */
	public void setDisplayNameVisible(boolean b){
		this.setMetadata(EntityMeta.ENTITY_CUSTOM_NAME_VISIBLE,
				ProtocolUtil.fromBoolean(b));
	}

	/**
	 * Checks if the display name is visible
	 *
	 * @return The result
	 */
	public boolean isDisplayNameVisible(){
		return (boolean) this.getMetadata(EntityMeta.ENTITY_CUSTOM_NAME_VISIBLE);
	}

	/**
	 * Set the display name
	 *
	 * @param name The string
	 */
	public void setDisplayName(String name){
		this.setMetadata(EntityMeta.ENTITY_CUSTOM_NAME, ChatUtil.colored(name));
	}

	/**
	 * Gets the display name
	 *
	 * @return The display name
	 */
	public String getDisplayName(){
		String dn = (String) this.getMetadata(EntityMeta.ENTITY_CUSTOM_NAME);
		if(dn == null){
			return displayName;
		}
		return dn;
	}

	/**
	 * Toggles the aimlock (look after player)
	 */
	public void toogleAimLock(){
		this.fixedAim = !this.fixedAim;
	}

	/**
	 * Get all nearbys players
	 *
	 * @param radius The radius
	 * @return The list of players nearby
	 */
	public List<Player> getNearbyPlayers(double radius){
		List<Player> players = new ArrayList<>();

		for(Player player : this.getLocation().getWorld().getPlayers()){
			if(player.getLocation().distance(this.getLocation()) <= radius){
				players.add(player);
			}
		}

		return players;
	}

	/**
	 * Updates the entity
	 */
	public void update(){
		this.despawn(false, getViewerAsPlayer());
		this.spawn(getViewerAsPlayer());

		for(FakeEntity p : getAttached()){
			p.update();
		}
	}

	/**
	 * Registers the npc at the npc registry
	 */
	public void register(){
		FakeEntityRegistry.register(this);

		for(FakeEntity p : getAttached()){
			p.register();
		}
	}

	/**
	 * -- Packet methods --
	 */

	public abstract void spawn(Player... players);

	/**
	 * Despawns the npc for given players
	 *
	 * @param constant If it should stay despawned
	 * @param players  The viewer
	 */
	public void despawn(boolean constant, Player... players){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_DESTROY);

		packet.get(int[].class).write(0, new int[]{getEntityId()});
		packet.send(players);

		if(constant){
			setViewers(false, players);
		}

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.despawn(constant, players);
		}
	}

	/**
	 * Updates the metadata of the datawatcher of this entity
	 */
	public void updateMetadata(){
		WrappedPacket packet = new WrappedPacket(
				PacketType.Play.Server.ENTITY_METADATA, getEntityId(), getDataWatcher().getHandle(), true);

//		packet.getIntegers().write(0, getEntityId());
//		packet.getWatchableObjectCollections().write(0, getDataWatcher().getWatchableObjectList());
		packet.send(getViewerAsPlayer());

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.updateMetadata();
		}
	}

	/**
	 * Update inventory content
	 */
	public void updateInventory(){
		List<WrappedPacket> packets = this.inventory.createPackets(getEntityId());
		if(packets.isEmpty()) return;

		for(WrappedPacket packet : packets){
			packet.send(getViewerAsPlayer());
		}

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.updateInventory();
		}
	}

	/**
	 * Attach given npc to this npc (synchronized movement)
	 *
	 * @param passengers The passengers
	 * @return This
	 */
	public FakeEntity addPassenger(FakeEntity... passengers){
		this.attached.addAll(Arrays.asList(passengers));
		return this;
	}

	/**
	 * Sets a metadata mobType with given object
	 *
	 * @param metadata The metadata index
	 * @param object   The object
	 */
	public void setMetadata(EntityMeta metadata, Object object){
		this.dataWatcher.set(metadata, object);
	}

	/**
	 * Gets a metadata from given index
	 *
	 * @param meta The index
	 * @return The result
	 */
	public Object getMetadata(EntityMeta meta){
		return this.dataWatcher.get(meta);
	}

	/**
	 * Attach given entity as passenger
	 *
	 * @param passengerId The entity id of the passenger
	 */
	public void setPassenger(int passengerId, boolean leash){
		this.attachEntity(getEntityId(), passengerId, leash);
	}

	public void setPassenger(int passengerId){
		this.attachEntity(getEntityId(), passengerId, false);
	}

	/**
	 * Attach given entity as vehicle
	 *
	 * @param vehicleId The entity id of the vehicle
	 */
	public void setVehicle(int vehicleId, boolean leash){
		this.attachEntity(vehicleId, getEntityId(), leash);
	}

	public void setVehicle(int vehicleId){
		this.attachEntity(vehicleId, getEntityId(), false);
	}

	/**
	 * Attaches an entity to another
	 * @param vehicleId The vehicle id
	 * @param passengerId The passengerid
	 * @param leash Leash?
	 */
	private void attachEntity(int vehicleId, int passengerId, boolean leash){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ATTACH_ENTITY);

		packet.getIntegers().write(0, ProtocolUtil.fromBoolean(leash));
		packet.getIntegers().write(1, passengerId);
		packet.getIntegers().write(2, vehicleId);

		packet.send(getViewerAsPlayer());
	}

	/**
	 * Teleports this entity to given location
	 *
	 * @param newLocation The new location
	 */
	public void teleport(Location newLocation){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_TELEPORT);

		packet.getIntegers().write(0, getEntityId());
		packet.getIntegers().write(1, LocationUtil.toFixedPoint(newLocation.getX()));
		packet.getIntegers().write(2, LocationUtil.toFixedPoint(newLocation.getY()));
		packet.getIntegers().write(3, LocationUtil.toFixedPoint(newLocation.getZ()));
		packet.getBytes().write(0, (byte) LocationUtil.toAngle(newLocation.getYaw()));
		packet.getBytes().write(1, (byte) LocationUtil.toAngle(newLocation.getPitch()));
		packet.send(getViewerAsPlayer());

		this.location = newLocation;

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.teleport(newLocation);
		}
	}

	/**
	 * Moves this entity to given location
	 *
	 * @param newLocation The new location
	 */
	public void move(Location newLocation){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.REL_ENTITY_MOVE);

		packet.getIntegers().write(0, getEntityId());
		packet.getBytes().write(1, (byte) LocationUtil.toFixedPoint(newLocation.getX() - getLocation().getX()));
		packet.getBytes().write(2, (byte) LocationUtil.toFixedPoint(newLocation.getY() - getLocation().getY()));
		packet.getBytes().write(3, (byte) LocationUtil.toFixedPoint(newLocation.getZ() - getLocation().getZ()));
		packet.getBooleans().write(0, true);
		packet.send(getViewerAsPlayer());

		this.location = newLocation;

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.move(newLocation);
		}
	}

	/**
	 * Moves and rotates this entity to given location
	 *
	 * @param newLocation The new location
	 * @param onGround    If the entity moves on ground
	 */
	public void moveAndLook(Location newLocation, boolean onGround){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_MOVE_LOOK);

		packet.getIntegers().write(0, getEntityId());
		packet.getBytes().write(0, (byte) LocationUtil.toFixedPoint(newLocation.getX() - getLocation().getX()));
		packet.getBytes().write(1, (byte) LocationUtil.toFixedPoint(newLocation.getY() - getLocation().getY()));
		packet.getBytes().write(2, (byte) LocationUtil.toFixedPoint(newLocation.getZ() - getLocation().getZ()));
		packet.getBytes().write(3, (byte) LocationUtil.toAngle(newLocation.getYaw()));
		packet.getBytes().write(4, (byte) LocationUtil.toAngle(newLocation.getPitch()));
		packet.getBooleans().write(0, onGround);
		packet.send(getViewerAsPlayer());

		this.location = newLocation;

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.moveAndLook(newLocation, onGround);
		}
	}

	/**
	 * Rotates this entity to given values
	 *
	 * @param yaw      The vertical rotation
	 * @param pitch    The horizontal rotation
	 * @param onGround If the entity is on ground
	 */
	public void rotate(double yaw, double pitch, boolean onGround){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_LOOK);
		this.location.setYaw((float) yaw);
		this.location.setPitch((float) pitch);

		packet.getIntegers().write(0, getEntityId());
		packet.getBytes().write(0, (byte) LocationUtil.toAngle(yaw));
		packet.getBytes().write(1, (byte) LocationUtil.toAngle(pitch));
		packet.getBooleans().write(0, onGround);
		packet.send(getViewerAsPlayer());

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.rotate(yaw, pitch, onGround);
		}
	}

	/**
	 * Rotates the head of this entity to given yaw
	 *
	 * @param headYaw The vertical rotation
	 */
	public void rotateHead(float headYaw){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.ENTITY_HEAD_ROTATION);

		packet.getIntegers().write(0, getEntityId());
		packet.getBytes().write(0, (byte) LocationUtil.toAngle(headYaw));
		packet.send(getViewerAsPlayer());

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.rotateHead(headYaw);
		}
	}

	public void rotate(Vector to){
		float rotation = LocationUtil.getLocalAngle(getLocation().toVector(), to);
		this.rotateHead(rotation);

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.rotate(to);
		}
	}

	/**
	 * Sets given player as viewer
	 *
	 * @param flag    Remove or add
	 * @param players The players
	 */
	public void setViewers(boolean flag, Player... players){
		for(Player p : players){
			if(flag){
				if(!isViewer(p))
					viewers.add(p.getUniqueId());
			}
			else{
				if(isViewer(p))
					viewers.remove(p.getUniqueId());
			}
		}

		// Synchronize attached npcs
		for(FakeEntity p : getAttached()){
			p.setViewers(flag, players);
		}
	}

	/**
	 * Checks if given player is a viewer
	 *
	 * @param player The player
	 * @return The result
	 */
	public boolean isViewer(Player player){
		return viewers.contains(player.getUniqueId());
	}

	/**
	 * Get all viewers as player (converting from uuid to player)
	 *
	 * @return The list of player
	 */
	public Player[] getViewerAsPlayer(){
		List<Player> list = new ArrayList<>();

		for(UUID id : getViewers()){
			list.add(Bukkit.getPlayer(id));
		}
		return list.toArray(new Player[]{});
	}

}
