package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.protocol.FakeEntityType;
import de.superioz.sx.bukkit.common.protocol.EntityMeta;
import de.superioz.sx.bukkit.common.protocol.ProtocolUtil;
import de.superioz.sx.bukkit.common.protocol.WrappedDataWatcher;
import de.superioz.sx.bukkit.listener.FakeMobListener;
import de.superioz.sx.bukkit.listener.ProtocolListener;
import org.bukkit.Bukkit;

/**
 * Created on 01.04.2016.
 */
public class FakeEntityHandler {

	/**
	 * Initialises the npc handler
	 */
	public static void init(){
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(BukkitLibrary.plugin(), new EntityAimUpdate(), 2L, 2L);

		// Register listener
		BukkitLibrary.pluginManager().registerEvents(new FakeMobListener(), BukkitLibrary.plugin());

		// ProtocolListener
		BukkitLibrary.addPacketHandler(new ProtocolListener());
	}

	/**
	 * Gets a datawatcher with default values for given entity mobType
	 *
	 * @param type The mobType
	 */
	public static void setDefaultWatchableObjects(WrappedDataWatcher dataWatcher, FakeEntityType type){
		dataWatcher.set(EntityMeta.ENTITY_STATE, (byte)0);
		dataWatcher.set(EntityMeta.ENTITY_IN_AIR, 300);
		dataWatcher.set(EntityMeta.ENTITY_IS_SILENT, ProtocolUtil.fromBoolean(false));
		dataWatcher.set(EntityMeta.ENTITY_CUSTOM_NAME_VISIBLE, ProtocolUtil.fromBoolean(true));


		if(type.isLiving()){
			dataWatcher.set(EntityMeta.LIVING_HEALTH, 1.0F);
			dataWatcher.set(EntityMeta.LIVING_POTION_EFFECT_COLOR, 0);
			dataWatcher.set(EntityMeta.LIVING_IS_POTION_EFFECT_AMBIENT, ProtocolUtil.fromBoolean(false));
			dataWatcher.set(EntityMeta.LIVING_NUMBER_OF_ARROWS_INSIDE, (byte) 0);
		}

		// Get datawatcherobjects from entitytype
		switch(type){
			case BAT:
				dataWatcher.set(EntityMeta.BAT_IS_HANGING, (byte) 0);
				break;
			case BLAZE:
				dataWatcher.set(EntityMeta.BLAZE_IS_ON_FIRE, (byte) 0);
				break;
			case SPIDER:
			case CAVE_SPIDER:
				dataWatcher.set(EntityMeta.SPIDER_IS_CLIMBING, (byte) 0);
				break;
			case CREEPER:
				dataWatcher.set(EntityMeta.CREEPER_STATE, (byte) -1);
				dataWatcher.set(EntityMeta.CREEPER_IS_POWERED, ProtocolUtil.fromBoolean(false));
				break;
			case ENDERMAN:
				dataWatcher.set(EntityMeta.ENDERMAN_CARRIED_BLOCK, (short) 0);
				dataWatcher.set(EntityMeta.ENDERMAN_CARRIED_BLOCK_DATA, (byte) 0);
				dataWatcher.set(EntityMeta.ENDERMAN_IS_SCREAMING, ProtocolUtil.fromBoolean(false));
				break;
			case ENDER_DRAGON:
				break;
			case GHAST:
				dataWatcher.set(EntityMeta.GHAST_IS_ATTACKING, ProtocolUtil.fromBoolean(false));
				break;
			case GIANT:
				break;
			case HORSE:
				dataWatcher.set(EntityMeta.HORSE_STATE, 0);
				dataWatcher.set(EntityMeta.HORSE_VARIANT, (byte) 0);
				dataWatcher.set(EntityMeta.HORSE_COLOR_STYLE, 0);
				dataWatcher.set(EntityMeta.HORSE_OWNER, "");
				dataWatcher.set(EntityMeta.HORSE_ARMOR, 0);
				break;
			case IRON_GOLEM:
				dataWatcher.set(EntityMeta.IRONGOLEM_IS_PLAYER_MADE, (byte) 0);
				break;
			case SLIME:
			case MAGMA_CUBE:
				dataWatcher.set(EntityMeta.SLIME_SIZE, (byte)1);
				break;
			case OCELOT:
				dataWatcher.set(EntityMeta.OCELOT_TYPE, (byte)0);
				break;
			case PIG:
				dataWatcher.set(EntityMeta.PIG_HAS_SADDLE, ProtocolUtil.fromBoolean(false));
				break;
			case PIG_ZOMBIE:
			case ZOMBIE:
				dataWatcher.set(EntityMeta.ZOMBIE_IS_BABY, ProtocolUtil.fromBoolean(false));
				dataWatcher.set(EntityMeta.ZOMBIE_IS_CONVERTING, ProtocolUtil.fromBoolean(false));
				dataWatcher.set(EntityMeta.ZOMBIE_IS_VILLAGER, ProtocolUtil.fromBoolean(false));
				break;
			case RABBIT:
				dataWatcher.set(EntityMeta.RABBIT_TYPE, (byte)0);
				break;
			case SHEEP:
				dataWatcher.set(EntityMeta.SHEEP_STATUS, (byte) 0);
				break;
			case WITHER_SKELETON:
				dataWatcher.set(EntityMeta.SKELETON_TYPE, (byte)1);
				break;
			case SKELETON:
				dataWatcher.set(EntityMeta.SKELETON_TYPE, (byte)0);
				break;
			case VILLAGER:
				dataWatcher.set(EntityMeta.VILLAGER_PROFESSION, 0);
				break;
			case WITCH:
				dataWatcher.set(EntityMeta.WITCH_IS_AGRESSIVE, ProtocolUtil.fromBoolean(false));
				break;
			case WITHER:
				dataWatcher.set(EntityMeta.WITHER_INVULNERABLE_TIME, 0);
				dataWatcher.set(EntityMeta.WITHER_TARGET_FIRST, 0);
				break;
			case WOLF:
				dataWatcher.set(EntityMeta.WOLF_HEALTH, 20F);
				dataWatcher.set(EntityMeta.WOLF_IS_BEGGING, ProtocolUtil.fromBoolean(false));
				dataWatcher.set(EntityMeta.WOLF_COLLAR_COLOR, (byte) 14);
				break;
			default:
				break;
		}
	}

}
