package de.superioz.sx.bukkit.common.render;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.util.BukkitUtilities;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created on 24.04.2016.
 */
@Getter
public class RenderQueueTask extends BukkitRunnable implements Listener {

	private Map<UUID, Queue<Short>> status = new HashMap<>();
	private final int mapsPerRun;

	public RenderQueueTask(int mapsPerSend){
		this.mapsPerRun = mapsPerSend;
		BukkitLibrary.registerListener(this);
	}

	@Override
	public void run(){
		if(MapImageRegistry.renderQueueList.isEmpty())
			return;

		for(Player p : BukkitUtilities.onlinePlayers()){
			Queue<Short> state = getStatus(p);

			for(int i = 0; i < mapsPerRun && !state.isEmpty(); i++){
				p.sendMap(Bukkit.getServer().getMap(state.poll()));
			}
		}
	}

	/**
	 * Add given map id to queue
	 *
	 * @param mapId The map id
	 */
	public void addToQueue(short mapId){
		for(Queue<Short> queue : status.values()){
			queue.add(mapId);
		}
	}

	/**
	 * Get queue of given player
	 *
	 * @param p The player
	 * @return The queue
	 */
	private Queue<Short> getStatus(Player p){
		if(!status.containsKey(p.getUniqueId()))
			status.put(p.getUniqueId(), new LinkedList<>(MapImageRegistry.renderQueueList));

		return status.get(p.getUniqueId());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		status.put(e.getPlayer().getUniqueId(), new LinkedList<>(MapImageRegistry.renderQueueList));
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		status.remove(e.getPlayer().getUniqueId());
	}

}