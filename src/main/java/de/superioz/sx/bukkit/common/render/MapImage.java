package de.superioz.sx.bukkit.common.render;

import lombok.Getter;

/**
 * Created on 24.04.2016.
 */
@Getter
public class MapImage {

	private String imageName;
	private String url;
	private int locationX;
	private int locationY;
	private short mapId;

	public MapImage(String imageName, String url, int locationX, int locationY, short mapId){
		this.imageName = imageName;
		this.url = url;
		this.locationX = locationX;
		this.locationY = locationY;
		this.mapId = mapId;
	}

	/**
	 * Checks if another map image is similar to this
	 *
	 * @param imageName The image name
	 * @param x         The x
	 * @param y         The y
	 * @return The result
	 */
	public boolean isSimilar(String imageName, int x, int y){
		return imageName.equalsIgnoreCase(imageName)
				&& x == getLocationX() && y == getLocationY();
	}

}
