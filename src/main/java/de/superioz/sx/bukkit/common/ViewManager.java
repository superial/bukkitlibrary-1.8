package de.superioz.sx.bukkit.common;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.common.protocol.*;
import de.superioz.sx.java.util.ReflectionUtils;
import de.superioz.sx.java.util.TimeUtils;
import de.superioz.sx.bukkit.util.ChatUtil;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Class created on April in 2015
 */
public class ViewManager {

	/**
	 * Send title to given players
	 *
	 * @param title           The title
	 * @param subtitle        The subtitle
	 * @param fadeIn          Fadein time
	 * @param stay            Stay time
	 * @param fadeOut         Fadeout time
	 * @param timeUnitSeconds Seconds or ticks?
	 * @param players         The players
	 */
	public static void sendTitle(String title, String subtitle, int fadeIn,
	                             int stay, int fadeOut, boolean timeUnitSeconds, Player... players){
		new CraftTitle(title, subtitle, new int[]{fadeIn, stay, fadeOut}).send(timeUnitSeconds, players);
	}

	/**
	 * Send title to given players
	 *
	 * @param title    The title
	 * @param subtitle The subtitle
	 * @param players  The players
	 */
	public static void sendTitle(String title, String subtitle, Player... players){
		sendTitle(title, subtitle, 1, 1, 1, true, players);
	}

	/**
	 * Send title to given players
	 *
	 * @param title   The title
	 * @param players The players
	 */
	public static void sendTitle(String title, Player... players){
		sendTitle(title, "", players);
	}

	/**
	 * Set sight for given spectator
	 *
	 * @param camera    The camera
	 * @param spectator The spectator who gets given camera
	 */
	public static void setSight(Player camera, Player spectator){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.CAMERA);
		packet.getIntegers().write(0, camera.getEntityId());

		packet.send(spectator);
	}

	/**
	 * Refreshes the player character
	 *
	 * @param player The player
	 */
	public static void refreshPlayer(Player player, Player... players){
		for(Player p : players){
			if(p != player)
				p.hidePlayer(player);
		}
		for(Player p : players){
			if(p != player)
				p.showPlayer(player);
		}
	}

	/**
	 * Updates the own player with respawn packet
	 *
	 * @param player The player
	 */
	public static void updateSelf(final Player player){
		WrappedPacket respawn = null;

		// Respawn packet
		try{
			Difficulty d = player.getWorld().getDifficulty();
			EnumWrappers.PlayerDifficulty playerDifficulty = EnumWrappers.PlayerDifficulty.values()[d.ordinal()];
			Method getById = ReflectionUtils.getMethod(ProtocolUtil.ENUM_GAMEMODE, "getById", int.class);
			Object gameMode = getById.invoke(null, player.getGameMode().getValue());
			Object[] types = (Object[]) ReflectionUtils.getField(ProtocolUtil.WORLD_TYPE, null, "types");
			assert types != null;
			Object worldType = types[0];

			respawn = new WrappedPacket(PacketType.Play.Server.RESPAWN);
			respawn.getIntegers().write(0, 0);
			respawn.getPlayerDifficulties().write(0, playerDifficulty);
			respawn.get(ProtocolUtil.ENUM_GAMEMODE).write(0, gameMode);
			respawn.get(ProtocolUtil.WORLD_TYPE).write(0, worldType);
		}
		catch(IllegalAccessException | InvocationTargetException e){
			e.printStackTrace();
		}

		// Values
		final boolean flying = player.isFlying();
		final Location location = player.getLocation();
		final int level = player.getLevel();
		final float xp = player.getExp();

		// Update
		final WrappedPacket finalRespawn = respawn;
		new BukkitRunnable() {
			@Override
			public void run(){
				finalRespawn.send(player);

				player.setFlying(flying);
				player.teleport(location);
				player.updateInventory();
				player.setLevel(level);
				player.setExp(xp);
			}
		}.runTaskLater(BukkitLibrary.plugin(), 5L);
	}

	/**
	 * Send hot bar message to given player
	 *
	 * @param text   The text
	 * @param player The player
	 */
	public static void sendHotbarMessage(String text, Player... player){
		byte target = (byte) 2;

		// Get protocol
		WrappedPacket wrappedPacket = new WrappedPacket(PacketType.Play.Server.CHAT);
		wrappedPacket.getChatComponents().write(0, new WrappedChatComponent(ChatUtil.colored(text)));
		wrappedPacket.getBytes().write(0, target);

		wrappedPacket.send(player);
	}

	/**
	 * Class created on April in 2015
	 */
	private static class CraftTitle {

		protected String title;
		protected String subTitle;
		protected int[] timings;

		public CraftTitle(String title, String subTitle, int[] timings){
			this.title = title;
			this.subTitle = subTitle;
			this.setTimings(timings);
		}

		/**
		 * Set the timings for this
		 *
		 * @param timings The timings as array
		 */
		public void setTimings(int[] timings){
			int[] timeValues = timings;

			if(timings != null && timings.length != 3){
				timeValues = new int[]{1, 1, 1};
			}

			this.timings = timeValues;
		}

		/**
		 * Get the protocol
		 *
		 * @return The container
		 */
		public WrappedPacket getPacket(){
			return new WrappedPacket(PacketType.Play.Server.TITLE);
		}

		/**
		 * Send protocol to given players
		 *
		 * @param ticks   The ticks
		 * @param players The players
		 */
		public void send(boolean ticks, Player... players){
			// Timings
			if(timings != null && timings.length == 3){
				WrappedPacket timingsPacket = getPacket();

				// Title
				timingsPacket.getTitleActions().write(0, EnumWrappers.TitleAction.TIMES);
				timingsPacket.getIntegers().write(0, TimeUtils.convertTime(timings[0], ticks));
				timingsPacket.getIntegers().write(1, TimeUtils.convertTime(timings[1], ticks));
				timingsPacket.getIntegers().write(2, TimeUtils.convertTime(timings[2], ticks));


				// Send
				timingsPacket.send(players);
			}

			// Title
			if(title != null && !title.isEmpty()){
				WrappedPacket titlePacket = getPacket();

				// Data
				titlePacket.getTitleActions().write(0, EnumWrappers.TitleAction.TITLE);
				titlePacket.getChatComponents().write(0, new WrappedChatComponent(ChatUtil.colored(title)));

				// Send
				titlePacket.send(players);
			}

			// Subtitle
			if(subTitle != null && !subTitle.isEmpty()){
				WrappedPacket subtitlePacket = getPacket();

				// Title
				subtitlePacket.getTitleActions().write(0, EnumWrappers.TitleAction.SUBTITLE);
				subtitlePacket.getChatComponents().write(0, new WrappedChatComponent(ChatUtil.colored(subTitle)));

				// Send
				subtitlePacket.send(players);
			}
		}

	}
}
