package de.superioz.sx.bukkit.common.command.verification;

import lombok.Getter;
import org.bukkit.entity.Player;

/**
 * Created on 10.04.2016.
 */
@Getter
public class MethodVerifierEvent {

	private boolean result;
	private Player player;

	public MethodVerifierEvent(boolean result, Player player){
		this.result = result;
		this.player = player;
	}

}
