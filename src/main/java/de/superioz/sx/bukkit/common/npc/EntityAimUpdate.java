package de.superioz.sx.bukkit.common.npc;

import org.bukkit.entity.Player;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class EntityAimUpdate implements Runnable {

	@Override
	public void run(){
		for(FakeEntity entity : FakeEntityRegistry.getRegistry()){
			if(entity.isFixedAim()) return;

			for(Player p : entity.getNearbyPlayers(5D)){
				entity.rotate(p.getEyeLocation().toVector());
			}
		}
	}

}
