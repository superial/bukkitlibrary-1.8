package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.BukkitLibrary;
import de.superioz.sx.bukkit.event.NPCUpdateEvent;
import de.superioz.sx.bukkit.util.BukkitUtilities;
import de.superioz.sx.bukkit.util.LocationUtil;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 01.04.2016.
 */
public class FakeEntityRegistry {

	private static List<FakeEntity> registry = new ArrayList<>();

	// Get the npc registry
	public static List<FakeEntity> getRegistry(){
		return registry;
	}

	/**
	 * Registers given npc
	 *
	 * @param entity The npc
	 */
	public static void register(FakeEntity entity){
		if(!isRegistered(entity))
			registry.add(entity);
	}

	/**
	 * Unregisters given npc
	 *
	 * @param entity The npc
	 */
	public static void unregister(FakeEntity entity){
		if(isRegistered(entity)){
			registry.remove(entity);
			entity.despawn(true, entity.getViewerAsPlayer());
		}
	}

	/**
	 * Checks if given npc is registered
	 *
	 * @param entity The npc
	 * @return The result
	 */
	public static boolean isRegistered(FakeEntity entity){
		return registry.contains(entity);
	}

	/**
	 * Unregisters all entities
	 */
	public static void unregisterAll(){
		for(FakeEntity e : getRegistry()){
			unregister(e);
		}
		registry = new ArrayList<>();
	}

	/**
	 * Gets all entities whose viewers contains given player
	 *
	 * @param player The player
	 *
	 * @return The list
	 */
	public static List<FakeEntity> getEntities(Player player){
		List<FakeEntity> entities = new ArrayList<>();

		for(FakeEntity e : getRegistry()){
			if(e.getViewers().contains(player.getUniqueId())){
				entities.add(e);
			}
		}

		return entities;
	}

	/**
	 * Updates all entities of given players
	 *
	 * @param players The players
	 */
	public static void updatePlayerView(boolean forSure, Player... players){
		for(Player p : players){
			for(FakeEntity e : getEntities(p)){
				double distance = p.getLocation().distance(e.getLocation());

				if(!forSure && !LocationUtil.checkRoughRange(distance, BukkitUtilities.CHUNK_WIDTH*4,
						BukkitUtilities.CHUNK_WIDTH)){
					return;
				}

				// Update entity
				e.update();
				BukkitLibrary.callEvent(new NPCUpdateEvent(p, e));
			}
		}
	}

	public static void updatePlayerView(Player... players){
		updatePlayerView(false, players);
	}

	/**
	 * Gets the npc of given id
	 *
	 * @param id The id
	 *
	 * @return The protocol
	 */
	public static FakeEntity getNPC(int id){
		for(FakeEntity entity : getRegistry()){
			if(entity.getEntityId() == id)
				return entity;
		}
		return null;
	}

}
