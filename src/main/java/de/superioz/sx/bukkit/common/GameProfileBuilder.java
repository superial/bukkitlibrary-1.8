package de.superioz.sx.bukkit.common;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.superioz.sx.bukkit.common.protocol.WrappedGameProfile;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.logging.Level;

/**
 * Created on 03.04.2016.
 */
public class GameProfileBuilder {

	private static Map<String, Skin> cache = new HashMap<>();

	/**
	 * Gets a gameprofile from given name
	 *
	 * @param name The name
	 * @return The profile
	 */
	public static WrappedGameProfile get(String name, String displayName){
		if(cache.containsKey(name))
			return new WrappedGameProfile(cache.get(name).toProfile(displayName));
		else{
			Skin skin = new Skin(name);
			if(!skin.isResult()){
				return null;
			}
			cache.put(name, skin);
			return new WrappedGameProfile(skin.toProfile(displayName));
		}
	}

	@Getter
	private static class Skin {

		private String uuid;
		private UUID uniqueId;
		private String name;
		private String value;
		private String signatur;
		private String playername;
		private boolean result;

		private OfflinePlayer player;

		/**
		 * The constructor with given uuid
		 *
		 * @param name The name
		 */
		public Skin(String name){
			player = Bukkit.getOfflinePlayer(name);
			UUID uniqueId = player.getUniqueId();
			this.uuid = uniqueId.toString().replaceAll("-", "");
			this.uniqueId = uniqueId;
			result = load();
		}

		/**
		 * Returns the skin as profile
		 *
		 * @return The profile
		 */
		public GameProfile toProfile(String name){
			GameProfile profile = new GameProfile(getUniqueId(), name);
			profile.getProperties().clear();
			profile.getProperties().put(getName(), new Property(getName(), getValue(), getSignatur()));

			return profile;
		}

		/**
		 * Loads the skin
		 */
		private boolean load(){
			try{
				playername = getPlayer().getName();

				URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
				URLConnection uc = url.openConnection();
				uc.setUseCaches(false);
				uc.setDefaultUseCaches(false);
				uc.addRequestProperty("User-Agent", "Mozilla/5.0");
				uc.addRequestProperty("Cache-Control", "no-cache, no-store, must-revalidate");
				uc.addRequestProperty("Pragma", "no-cache");

				// Parse it
				String json = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A").next();
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(json);
				JSONArray properties = (JSONArray) ((JSONObject) obj).get("properties");
				for(Object pro : properties){
					try{
						JSONObject property = (JSONObject) pro;
						String name = (String) property.get("name");
						String value = (String) property.get("value");
						String signature = property.containsKey("signature") ? (String) property.get("signature") : null;


						this.name = name;
						this.value = value;
						this.signatur = signature;
					}
					catch(Exception e){
						Bukkit.getLogger().log(Level.WARNING, "Failed to apply auth property", e);
					}
				}
				uc.setConnectTimeout(0);
				uc.getInputStream().close();
				return true;
			}
			catch(Exception e){
				return false;
			}
		}

	}

}
