package de.superioz.sx.bukkit.common;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.superioz.sx.bukkit.common.protocol.*;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created on 26.04.2016.
 */
public class NickRegistry {

	private static Map<UUID, Collection<Property>> profiles = new HashMap<>();
	private static Map<UUID, String> oldNames = new HashMap<>();

	/**
	 * Get old profile from given uuid
	 *
	 * @param uuid The uuid
	 * @return The profile
	 */
	public static Collection<Property> getOldProfile(UUID uuid){
		Collection<Property> profile = profiles.get(uuid);

		if(profile == null) return null;
		return profile;
	}

	/**
	 * Get old name from given uuid
	 *
	 * @param uuid The uuid
	 * @return The name
	 */
	public static String getOldName(UUID uuid){
		return oldNames.get(uuid);
	}

	/**
	 * Puts into both maps
	 *
	 * @param player The player
	 */
	public static void put(Player player){
		UUID uuid = player.getUniqueId();
		if(contains(uuid)){
			return;
		}

		WrappedGameProfile profile = WrappedGameProfile.from(player);
		String name = player.getName();
		profiles.put(uuid, new ArrayList<>(profile.getProperties().get("textures")));
		oldNames.put(uuid, name);
	}

	/**
	 * Removes given player from maps
	 *
	 * @param player The player
	 */
	public static void remove(Player player){
		UUID uuid = player.getUniqueId();
		if(!contains(uuid)){
			return;
		}

		profiles.remove(uuid);
		oldNames.remove(uuid);
	}

	/**
	 * Checks if given uuid is present
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public static boolean contains(UUID uuid){
		return profiles.containsKey(uuid) && oldNames.containsKey(uuid);
	}

	// ==

	/**
	 * Nicks the player
	 *
	 * @param player      The player
	 * @param gameProfile The game profile
	 * @param type        The type
	 * @param viewers     The viewers
	 */
	private static void nickPlayer(Player player, WrappedGameProfile gameProfile,
	                               NickPacket.Type type, boolean r, Player... viewers){
		UUID uuid = player.getUniqueId();
		NickPacket packet = new NickPacket(player, gameProfile, type);

		if(!contains(uuid)){
			put(player);
		}
		packet.send(r, viewers);
	}

	public static void nickPlayer(Player player, WrappedGameProfile gameProfile, boolean r, Player... viewers){
		nickPlayer(player, gameProfile, NickPacket.Type.ADD, r, viewers);
	}

	public static void unickPlayer(Player player, Player... viewers){
		UUID uuid = player.getUniqueId();
		Collection<Property> oldProfile = getOldProfile(uuid);

		if(oldProfile != null){
			GameProfile profile = new GameProfile(uuid, getOldName(uuid));
			profile.getProperties().removeAll("textures");
			profile.getProperties().putAll("textures", oldProfile);

			nickPlayer(player, new WrappedGameProfile(profile), NickPacket.Type.RESET, true, viewers);
			NickRegistry.remove(player);
		}
	}


	/**
	 * Created on 26.04.2016.
	 */
	@Getter
	public static class NickPacket {

		private Player player;
		private WrappedGameProfile gameProfile;
		private WrappedGameProfile currentProfile;
		private Type type;

		private WrappedPacket removeTab;
		private WrappedPacket addTab;

		public NickPacket(Player player, WrappedGameProfile gameProfile, Type type){
			this.player = player;
			this.gameProfile = gameProfile;
			this.currentProfile = WrappedGameProfile.from(player);
			this.type = type;
		}

		/**
		 * Send nick packet to given players
		 *
		 * @param players The players
		 */
		public void send(boolean refresh, Player... players){
			// Remove from tablist
			if(refresh){
				this.constructRemovePacket();
				this.getRemoveTab().send(players);
			}

			// Set properties
			Collection<Property> properties = new ArrayList<>(getGameProfile().getProperties().get("textures"));

			System.out.println(properties.size());
			currentProfile.getProperties().removeAll("textures");
			System.out.println(properties.size());
			currentProfile.getProperties().putAll("textures", properties);
			System.out.println(properties.size());
			currentProfile.setName(getGameProfile().getName());

			//
			currentProfile.deepInsert(player);

			// Update view
			if(refresh){
				ViewManager.updateSelf(player);
				ViewManager.refreshPlayer(player, players);
			}

			// Add to tablist
			if(refresh){
				this.constructAddPacket();
				this.getAddTab().send(players);
			}
		}

		/**
		 * Construct the remove packet
		 */
		private void constructRemovePacket(){
			// Remove tablist packet
			removeTab = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);
			removeTab.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
			removeTab.getPlayerInfoDataLists().write(0, Collections.singletonList(new PlayerInfoData(
					EnumWrappers.NativeGameMode.NOT_SET, getCurrentProfile(), 0,
					new WrappedChatComponent(getCurrentProfile().getName()))));
		}

		/**
		 * Construct the add packet
		 */
		private void constructAddPacket(){
			// Add tablist packet
			addTab = new WrappedPacket(PacketType.Play.Server.PLAYER_INFO);
			addTab.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
			addTab.getPlayerInfoDataLists().write(0, Collections.singletonList(new PlayerInfoData(
					EnumWrappers.NativeGameMode.NOT_SET, getCurrentProfile(), 0,
					new WrappedChatComponent(getCurrentProfile().getName()))));
		}

		/**
		 * Type of nicking (reset / add nick)
		 */
		public enum Type {

			RESET,
			ADD

		}

	}
}
