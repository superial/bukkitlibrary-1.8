package de.superioz.sx.bukkit.common.command;

import de.superioz.sx.java.util.SimpleStringUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 01.03.2016.
 */
@Getter
public class CommandFlag {

	public static final String SPECIFIER = "-";

	private String name;
	private boolean needArguments;
	private int maxArguments;
	private List<String> arguments;
	private CommandWrapper root;

	// default constructor
	public CommandFlag(CommandWrapper root, String name, boolean needArguments, int maxArguments){
		this.needArguments = needArguments;
		this.name = name;
		this.maxArguments = maxArguments;
		this.root = root;
	}

	public CommandFlag(CommandWrapper root, String name, boolean needArguments){
		this(root, name, needArguments, 1);
	}

	public CommandFlag(CommandWrapper root, String name){
		this(root, name, false);
	}

	/**
	 * Sets the arguments for this flag
	 *
	 * @param arguments The arguments
	 */
	public void setArguments(String[] arguments){
		List<String> args = new ArrayList<>();

		// Check if flag consists of infinite arguments (e.g a name)
		if(maxArguments < 0){
			maxArguments = arguments.length;
		}

		// Get the flag arguments from given string array
		for(int i = 0; i < maxArguments; i++){
			String s = arguments[i];

			if(s.startsWith("-")){
				String flag = s.replace("-", "");
				if(getRoot().getFlags().contains(flag)){
					break;
				}
				args.add(s);
			}
			else{
				args.add(s);
			}
		}
		this.arguments = args;
	}

	/**
	 * Get the arguments length
	 * @return The int
	 */
	public int getArgumentsLength(){
		return getArguments().size();
	}

	/**
	 * Get integer with given index
	 *
	 * @param index The index
	 * @return The int
	 */
	public int getInt(int index, int defaultValue){
		String arg;
		if(getArgumentsLength() < index
				|| !SimpleStringUtils.isInteger(arg = getArgument(index))
				|| arg.length() > (Integer.MAX_VALUE + "").length()){
			return defaultValue;
		}
		else{
			return Integer.parseInt(arg);
		}
	}

	/**
	 * Get boolean with given index
	 *
	 * @param index The index
	 * @return The int
	 */
	public boolean getBoolean(int index, boolean defaultValue){
		if(getArgumentsLength() < index){
			return defaultValue;
		}
		else{
			return Boolean.parseBoolean(getArgument(index));
		}
	}

	/**
	 * Get the argument from this flag
	 *
	 * @param index The index
	 * @return The argument
	 */
	public String getArgument(int index){
		if(index < 1 || index > getArguments().size())
			index = 1;

		return getArguments().get(index - 1);
	}

	/**
	 * Check if the flag doesn't contain an argument
	 *
	 * @return The result as boolean
	 */
	public boolean isEmpty(){
		return getArguments().size() == 0;
	}

	/**
	 * Checks if the name of this flag is equals given string
	 *
	 * @param otherFlagsName The name of the other flag (or just a string)
	 * @return The result
	 */
	public boolean isSimilar(String otherFlagsName){
		return getName().equals(otherFlagsName);
	}

}
