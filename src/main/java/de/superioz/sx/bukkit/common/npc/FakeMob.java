package de.superioz.sx.bukkit.common.npc;

import de.superioz.sx.bukkit.common.protocol.FakeEntityType;
import de.superioz.sx.bukkit.common.protocol.PacketType;
import de.superioz.sx.bukkit.common.protocol.WrappedPacket;
import de.superioz.sx.bukkit.util.LocationUtil;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created on 01.04.2016.
 */
@Getter
public class FakeMob extends FakeEntity {

	private int entityTypeId;
	private boolean fixedAim;

	/**
	 * Constructor for npcmob
	 *
	 * @param type        The entity mobType
	 * @param location    The location
	 * @param displayName The displayname
	 * @param fixedAim    Should he looks after players?
	 */
	public FakeMob(FakeEntityType type, Location location, String displayName, boolean fixedAim){
		super(type, location, displayName);
		this.entityTypeId = getType().getIdentifier();
		this.fixedAim = fixedAim;
	}

	public FakeMob(FakeEntityType type, Location location, String displayName){
		this(type, location, displayName, true);
	}

	/**
	 * Toggles the aim update
	 */
	public void toggleAimUpdate(){
		this.fixedAim = !fixedAim;
	}

	@Override
	public void spawn(Player... players){
		WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.SPAWN_ENTITY_LIVING);

		packet.getIntegers().write(0, getEntityId());
		packet.getIntegers().write(1, getEntityTypeId());
		packet.getIntegers().write(2, LocationUtil.toFixedPoint(getLocation().getX()));
		packet.getIntegers().write(3, LocationUtil.toFixedPoint(getLocation().getY() + 0.001D));
		packet.getIntegers().write(4, LocationUtil.toFixedPoint(getLocation().getZ()));
		packet.getBytes().write(0, (byte) LocationUtil.toAngle(getLocation().getYaw()));
		packet.getBytes().write(1, (byte) LocationUtil.toAngle(getLocation().getPitch()));
		packet.getBytes().write(2, (byte) LocationUtil.toAngle(getLocation().getYaw()));
		packet.getDataWatcherModifier().write(0, getDataWatcher());
		packet.send(players);

		// Update inventory
		this.updateInventory();

		// Set register
		super.setViewers(true, players);
		super.register();
	}

}
