package de.superioz.sx.bukkit.common.render;

import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created on 24.04.2016.
 */
public class MapImageRenderer extends MapRenderer {

	public static final int MAP_WIDTH = 128;
	public static final int MAP_HEIGHT = 128;
	private Image image = null;
	private boolean first = true;

	/**
	 * Class to render maps
	 *
	 * @param image The image
	 * @param x1    Horizontal value of subImage
	 * @param y1    Vertical value of subImage
	 */
	public MapImageRenderer(BufferedImage image, int x1, int y1){
		recalculateInput(image, x1, y1);
	}

	/**
	 * (Re)calculates the image
	 *
	 * @param input The image
	 * @param x1    Horizontal value of subImage
	 * @param y1    Vertical value of subImage
	 */
	public void recalculateInput(BufferedImage input, int x1, int y1){
		int x2 = MAP_WIDTH;
		int y2 = MAP_HEIGHT;
		if((x1 > input.getWidth()) || (y1 > input.getHeight())){
			return;
		}
		if(x1 + x2 >= input.getWidth()){
			x2 = input.getWidth() - x1;
		}
		if(y1 + y2 >= input.getHeight()){
			y2 = input.getHeight() - y1;
		}
		this.image = input.getSubimage(x1, y1, x2, y2);

		this.first = true;
	}

	/**
	 * Renders the map for given player
	 *
	 * @param view   The view
	 * @param canvas The canvas
	 */
	@Override
	public void render(MapView view, MapCanvas canvas, Player player){
		if((this.image != null) && (this.first)){
			canvas.drawImage(0, 0, this.image);
			this.first = false;
		}
	}

}
