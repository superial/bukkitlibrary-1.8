package de.superioz.sx.bukkit.common.render;

import de.superioz.sx.bukkit.BukkitLibrary;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ItemFrame;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 24.04.2016.
 */
public class MapImageRegistry {

	public static final int SEND_PER_TICKS = 20;
	public static final int MAPS_PER_SEND = 8;

	public static List<Short> renderQueueList = new ArrayList<>();
	public static List<MapImage> maps = new ArrayList<>();
	public static Map<String, BufferedImage> images = new HashMap<>();
	public static RenderQueueTask renderQueueTask;

	/**
	 * Initialises the registry
	 */
	public static void init(){
		renderQueueTask = new RenderQueueTask(MAPS_PER_SEND);
		renderQueueTask.runTaskTimer(BukkitLibrary.plugin(), SEND_PER_TICKS, SEND_PER_TICKS);
	}

	/**
	 * Start placing the image
	 *
	 * @param block     Block block
	 * @param face      The block face
	 * @param imageName The imageName
	 * @param url       The url
	 * @param queueTask As queue task?
	 */
	public static boolean startPlacing(Block block, BlockFace face,
	                                   String imageName, String url, boolean queueTask){
		MapCacheEntry entry = new MapCacheEntry(imageName, url, queueTask);

		if(!placeImage(block, face, entry)){
			return false;
		}
		else{
			// Save
			return true;
		}
	}

	/**
	 * Place image in itemframe at given location
	 *
	 * @param block The block
	 * @param face  The face
	 * @param entry The map entry
	 * @return The result
	 */
	public static boolean placeImage(Block block, BlockFace face, MapCacheEntry entry){
		int xMod = 0, zMod = 0;

		// Check block face
		switch(face){
			case EAST:
				zMod = -1;
				break;
			case WEST:
				zMod = 1;
				break;
			case SOUTH:
				xMod = 1;
				break;
			case NORTH:
				xMod = -1;
				break;
			default:
				System.out.println("Someone tried to create an image with an invalid block facing");
				return false;
		}

		BufferedImage image = loadImage(entry.getImageName(), entry.getUrl());
		if(image == null){
			System.out.println("Someone tried to create an image with an invalid file!");
			return false;
		}

		Block b = block.getRelative(face);
		int width = (int) Math.ceil(image.getWidth() / (double) MapImageRenderer.MAP_WIDTH);
		int height = (int) Math.ceil(image.getHeight() / (double) MapImageRenderer.MAP_HEIGHT);

		// Check field
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				Block relative1 = block.getRelative(x * xMod, -y, x * zMod);
				Block relative2 = block.getRelative(x * xMod - zMod, -y, x * zMod + xMod);
				if(!relative1.getType().isSolid()
						|| relative2.getType().isSolid()){
					System.out.println("Not enough space!");
					return false;
				}
			}
		}

		System.out.println("Height: " + height + "; Width: " + width);
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				Block relative = b.getRelative(x * xMod, -y, x * zMod);

				// Could throw nullpointer but should not
				try{
					setItemFrame(relative, image, face,
							x * MapImageRenderer.MAP_WIDTH, y * MapImageRenderer.MAP_HEIGHT, entry);
				}
				catch(NullPointerException ex){
					System.out.println("Unfortunatly this is caused be the way Minecraft/CraftBukkit handles the spawning of Entities.");
				}
			}
		}
		return true;
	}

	/**
	 * Set the item frame with given image to given values
	 *
	 * @param b     The block
	 * @param image The buffered image
	 * @param face  The block face
	 * @param x     The x location
	 * @param y     The y location
	 * @param entry The cache entry
	 */
	private static void setItemFrame(Block b, BufferedImage image, BlockFace face,
	                                 int x, int y, MapCacheEntry entry){
		ItemFrame frame = b.getWorld().spawn(b.getLocation(), ItemFrame.class);
		frame.setFacingDirection(face, false);

		ItemStack item = getMapItem(entry.getImageName(), x, y, image);
		frame.setItem(item);
		short id = item.getDurability();

		if(entry.isQueueTask() && !renderQueueList.contains(id)){
			renderQueueList.add(id);
			renderQueueTask.addToQueue(id);
		}
		maps.add(new MapImage(entry.getImageName(), entry.getUrl(), x, y, id));
	}

	/**
	 * Get the map item for given image values
	 *
	 * @param imageName The name of map image
	 * @param x         The subimage x coord
	 * @param y         The subimage y coord
	 * @param image     The buffered image
	 * @return The itemstack
	 */
	public static ItemStack getMapItem(String imageName, int x, int y, BufferedImage image){
		ItemStack item = new ItemStack(Material.MAP);

		// Loop through maps
		for(MapImage m : maps){
			if(m.isSimilar(imageName, x, y)){
				item.setDurability(m.getMapId());
				return item;
			}
		}

		// Set renderer
		MapView map = Bukkit.getServer().createMap(Bukkit.getServer().getWorlds().get(0));
		for(MapRenderer r : map.getRenderers()){
			map.removeRenderer(r);
		}
		map.addRenderer(new MapImageRenderer(image, x, y));

		item.setDurability(map.getId());
		return item;
	}

	/**
	 * Loads an image into the image cache
	 *
	 * @param imageName The identifier of the image
	 * @param url       The web url
	 * @return The bufferedImage
	 */
	public static BufferedImage loadImage(String imageName, String url){
		if(images.containsKey(imageName)){
			return images.get(imageName);
		}

		try{
			BufferedImage image;
			try{
				URL internetUrl = new URL(url);
				image = ImageIO.read(internetUrl);
			}
			catch(MalformedURLException e){
				image = ImageIO.read(new File(BukkitLibrary.plugin().getDataFolder() + url));
			}
			images.put(imageName, image);
			return image;
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Load given maps into cache etc.
	 *
	 * @param mapImages The maps
	 */
	public static void loadMaps(Map<MapImage, Boolean> mapImages){
		for(MapImage m : mapImages.keySet()){
			short id = m.getMapId();
			boolean queueTask = mapImages.get(m);
			int x = m.getLocationX();
			int y = m.getLocationY();

			MapView map = Bukkit.getServer().getMap(id);
			for(MapRenderer r : map.getRenderers()){
				map.removeRenderer(r);
			}
			BufferedImage image = loadImage(m.getImageName(), m.getUrl());

			if(image == null){
				continue;
			}
			if(queueTask){
				renderQueueList.add(id);
			}

			map.addRenderer(new MapImageRenderer(loadImage(m.getImageName(), m.getUrl()), x, y));
			maps.add(m);
		}
	}

	/**
	 * Reloads a given image
	 *
	 * @param imageName The image name
	 * @param url       The url
	 */
	public static void reloadImage(String imageName, String url){
		images.remove(imageName);
		BufferedImage image = loadImage(imageName, url);

		if(image == null){
			return;
		}
		int width = (int) Math.ceil((double) image.getWidth() / (double) MapImageRenderer.MAP_WIDTH);
		int height = (int) Math.ceil((double) image.getHeight() / (double) MapImageRenderer.MAP_HEIGHT);

		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				short id = getMapItem(imageName,
						x * MapImageRenderer.MAP_WIDTH, y * MapImageRenderer.MAP_HEIGHT, image).getDurability();
				MapView map = Bukkit.getServer().getMap(id);

				for(MapRenderer renderer : map.getRenderers()){
					if(renderer instanceof MapImageRenderer){
						((MapImageRenderer) renderer).recalculateInput(image,
								x * MapImageRenderer.MAP_WIDTH, y * MapImageRenderer.MAP_HEIGHT);
					}
				}
				renderQueueTask.addToQueue(id);
			}
		}
	}
}
