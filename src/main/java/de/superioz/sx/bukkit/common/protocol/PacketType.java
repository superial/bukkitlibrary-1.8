package de.superioz.sx.bukkit.common.protocol;

import de.superioz.sx.java.util.WrappedFieldArray;
import lombok.Getter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 31.03.2016.
 */
@Getter
public class PacketType implements Comparable<PacketType> {

	private String className;
	private Sender sender;
	private int id;

	private static List<PacketType> packetLookup = new ArrayList<>();

	static{
		registerPacketClass(new Play.Server());
		registerPacketClass(new Play.Client());
	}

	/**
	 * The constructor of the packet mobType
	 *
	 * @param sender    The packet sender
	 * @param className The packet class name
	 * @param id        The packet id
	 */
	public PacketType(Sender sender, String className, String enclosingClass, int id){
		this.sender = sender;
		this.id = id;

		String packetType = "PacketPlay" + ((sender == Sender.CLIENT) ? "In" : "Out");
		if(enclosingClass != null && !enclosingClass.isEmpty()){
			packetType = enclosingClass + "$" + packetType;
		}

		this.className = packetType + className;
	}

	public PacketType(Sender sender, String className, PacketType enclosingClass, int id){
		this(sender, className, enclosingClass.getClassName(), id);
	}

	public PacketType(Sender sender, String className, int id){
		this(sender, className, "", id);
	}

	/**
	 * Lookup a packet with id
	 *
	 * @param id The id
	 * @return The packet
	 */
	public static PacketType lookup(int id){
		for(PacketType type : packetLookup){
			if(type.getId() == id) return type;
		}
		throw new IllegalArgumentException("Couldn't find packet with ID='" + id + "'.");
	}

	/**
	 * Lookup a packet with class name
	 *
	 * @param className The class name
	 * @return The packet
	 */
	public static PacketType lookup(String className){
		for(PacketType type : packetLookup){
			String name = type.getClassName();
			String typeClassName = name.contains("$") ? name.split("\\$")[1] : name;

			if(typeClassName.equals(className)){
				return type;
			}
		}

		throw new IllegalArgumentException("Couldn't find packet with Name='" + className + "'.");
	}

	/**
	 * Registers given class instance into the packet lookup
	 *
	 * @param classInstance The instance
	 */
	private static void registerPacketClass(Object classInstance){
		WrappedFieldArray<PacketType> array = new WrappedFieldArray<>(classInstance, PacketType.class);

		try{
			for(Field f : array.getFields()){
				packetLookup.add((PacketType) f.get(null));
			}
		}
		catch(IllegalAccessException e){
			e.printStackTrace();
		}
	}

	@Override
	public int compareTo(PacketType o){
		return o.getClassName().compareTo(getClassName());
	}

	public static class Play {

		public static class Server {

			private static final PacketType.Sender SENDER;
			public static final PacketType KEEP_ALIVE;
			public static final PacketType LOGIN;
			public static final PacketType CHAT;
			public static final PacketType UPDATE_TIME;
			public static final PacketType ENTITY_EQUIPMENT;
			public static final PacketType SPAWN_POSITION;
			public static final PacketType UPDATE_HEALTH;
			public static final PacketType RESPAWN;
			public static final PacketType POSITION;
			public static final PacketType HELD_ITEM_SLOT;
			public static final PacketType BED;
			public static final PacketType ANIMATION;
			public static final PacketType NAMED_ENTITY_SPAWN;
			public static final PacketType COLLECT;
			public static final PacketType SPAWN_ENTITY;
			public static final PacketType SPAWN_ENTITY_LIVING;
			public static final PacketType SPAWN_ENTITY_PAINTING;
			public static final PacketType SPAWN_ENTITY_EXPERIENCE_ORB;
			public static final PacketType ENTITY_VELOCITY;
			public static final PacketType ENTITY_DESTROY;
			public static final PacketType ENTITY;
			public static final PacketType REL_ENTITY_MOVE;
			public static final PacketType ENTITY_LOOK;
			public static final PacketType ENTITY_MOVE_LOOK;
			public static final PacketType ENTITY_TELEPORT;
			public static final PacketType ENTITY_HEAD_ROTATION;
			public static final PacketType ENTITY_STATUS;
			public static final PacketType ATTACH_ENTITY;
			public static final PacketType ENTITY_METADATA;
			public static final PacketType ENTITY_EFFECT;
			public static final PacketType REMOVE_ENTITY_EFFECT;
			public static final PacketType EXPERIENCE;
			public static final PacketType UPDATE_ATTRIBUTES;
			public static final PacketType MAP_CHUNK;
			public static final PacketType MULTI_BLOCK_CHANGE;
			public static final PacketType BLOCK_CHANGE;
			public static final PacketType BLOCK_ACTION;
			public static final PacketType BLOCK_BREAK_ANIMATION;
			public static final PacketType MAP_CHUNK_BULK;
			public static final PacketType EXPLOSION;
			public static final PacketType WORLD_EVENT;
			public static final PacketType NAMED_SOUND_EFFECT;
			public static final PacketType WORLD_PARTICLES;
			public static final PacketType GAME_STATE_CHANGE;
			public static final PacketType SPAWN_ENTITY_WEATHER;
			public static final PacketType OPEN_WINDOW;
			public static final PacketType CLOSE_WINDOW;
			public static final PacketType SET_SLOT;
			public static final PacketType WINDOW_ITEMS;
			public static final PacketType CRAFT_PROGRESS_BAR;
			public static final PacketType TRANSACTION;
			public static final PacketType UPDATE_SIGN;
			public static final PacketType MAP;
			public static final PacketType TILE_ENTITY_DATA;
			public static final PacketType OPEN_SIGN_ENTITY;
			public static final PacketType STATISTICS;
			public static final PacketType PLAYER_INFO;
			public static final PacketType ABILITIES;
			public static final PacketType TAB_COMPLETE;
			public static final PacketType SCOREBOARD_OBJECTIVE;
			public static final PacketType SCOREBOARD_SCORE;
			public static final PacketType SCOREBOARD_DISPLAY_OBJECTIVE;
			public static final PacketType SCOREBOARD_TEAM;
			public static final PacketType CUSTOM_PAYLOAD;
			public static final PacketType KICK_DISCONNECT;
			public static final PacketType SERVER_DIFFICULTY;
			public static final PacketType COMBAT_EVENT;
			public static final PacketType CAMERA;
			public static final PacketType WORLD_BORDER;
			public static final PacketType TITLE;
			public static final PacketType SET_COMPRESSION;
			public static final PacketType PLAYER_LIST_HEADER_FOOTER;
			public static final PacketType RESOURCE_PACK_SEND;
			public static final PacketType UPDATE_ENTITY_NBT;

			static{
				SENDER = Sender.SERVER;
				KEEP_ALIVE = new PacketType(SENDER, "KeepAlive", 0);
				LOGIN = new PacketType(SENDER, "Login", 1);
				CHAT = new PacketType(SENDER, "Chat", 2);
				UPDATE_TIME = new PacketType(SENDER, "UpdateTime", 3);
				ENTITY_EQUIPMENT = new PacketType(SENDER, "EntityEquipment", 4);
				SPAWN_POSITION = new PacketType(SENDER, "SpawnPosition", 5);
				UPDATE_HEALTH = new PacketType(SENDER, "UpdateHealth", 6);
				RESPAWN = new PacketType(SENDER, "Respawn", 7);
				POSITION = new PacketType(SENDER, "Position", 8);
				HELD_ITEM_SLOT = new PacketType(SENDER, "HeldItemSlot", 9);
				BED = new PacketType(SENDER, "Bed", 10);
				ANIMATION = new PacketType(SENDER, "Animation", 11);
				NAMED_ENTITY_SPAWN = new PacketType(SENDER, "NamedEntitySpawn", 12);
				COLLECT = new PacketType(SENDER, "Collect", 13);
				SPAWN_ENTITY = new PacketType(SENDER, "SpawnEntity", 14);
				SPAWN_ENTITY_LIVING = new PacketType(SENDER, "SpawnEntityLiving", 15);
				SPAWN_ENTITY_PAINTING = new PacketType(SENDER, "SpawnEntityPainting", 16);
				SPAWN_ENTITY_EXPERIENCE_ORB = new PacketType(SENDER, "SpawnEntityExperienceOrb", 17);
				ENTITY_VELOCITY = new PacketType(SENDER, "EntityVelocity", 18);
				ENTITY_DESTROY = new PacketType(SENDER, "EntityDestroy", 19);
				ENTITY = new PacketType(SENDER, "Entity", 20);
				REL_ENTITY_MOVE = new PacketType(SENDER, "RelEntityMove", ENTITY, 21);
				ENTITY_LOOK = new PacketType(SENDER, "EntityLook", ENTITY, 22);
				ENTITY_MOVE_LOOK = new PacketType(SENDER, "RelEntityMoveLook", ENTITY, 23);
				ENTITY_TELEPORT = new PacketType(SENDER, "EntityTeleport", 24);
				ENTITY_HEAD_ROTATION = new PacketType(SENDER, "EntityHeadRotation", 25);
				ENTITY_STATUS = new PacketType(SENDER, "EntityStatus", 26);
				ATTACH_ENTITY = new PacketType(SENDER, "AttachEntity", 27);
				ENTITY_METADATA = new PacketType(SENDER, "EntityMetadata", 28);
				ENTITY_EFFECT = new PacketType(SENDER, "EntityEffect", 29);
				REMOVE_ENTITY_EFFECT = new PacketType(SENDER, "RemoveEntityEffect", 30);
				EXPERIENCE = new PacketType(SENDER, "Experience", 31);
				UPDATE_ATTRIBUTES = new PacketType(SENDER, "UpdateAttributes", 32);
				MAP_CHUNK = new PacketType(SENDER, "MapChunk", 33);
				MULTI_BLOCK_CHANGE = new PacketType(SENDER, "MultiBlockChange", 34);
				BLOCK_CHANGE = new PacketType(SENDER, "BlockChange", 35);
				BLOCK_ACTION = new PacketType(SENDER, "BlockAction", 36);
				BLOCK_BREAK_ANIMATION = new PacketType(SENDER, "BlockBreakAnimation", 37);
				MAP_CHUNK_BULK = new PacketType(SENDER, "MapChunkBulk", 38);
				EXPLOSION = new PacketType(SENDER, "Explosion", 39);
				WORLD_EVENT = new PacketType(SENDER, "WorldEvent", 40);
				NAMED_SOUND_EFFECT = new PacketType(SENDER, "NamedSoundEffect", 41);
				WORLD_PARTICLES = new PacketType(SENDER, "WorldParticles", 42);
				GAME_STATE_CHANGE = new PacketType(SENDER, "GameStateChange", 43);
				SPAWN_ENTITY_WEATHER = new PacketType(SENDER, "SpawnEntityWeather", 44);
				OPEN_WINDOW = new PacketType(SENDER, "OpenWindow", 45);
				CLOSE_WINDOW = new PacketType(SENDER, "CloseWindow", 46);
				SET_SLOT = new PacketType(SENDER, "SetSlot", 47);
				WINDOW_ITEMS = new PacketType(SENDER, "WindowItems", 48);
				CRAFT_PROGRESS_BAR = new PacketType(SENDER, "WindowData", 49);
				TRANSACTION = new PacketType(SENDER, "Transaction", 50);
				UPDATE_SIGN = new PacketType(SENDER, "UpdateSign", 51);
				MAP = new PacketType(SENDER, "Map", 52);
				TILE_ENTITY_DATA = new PacketType(SENDER, "TileEntityData", 53);
				OPEN_SIGN_ENTITY = new PacketType(SENDER, "OpenSignEditor", 54);
				STATISTICS = new PacketType(SENDER, "Statistic", 55);
				PLAYER_INFO = new PacketType(SENDER, "PlayerInfo", 56);
				ABILITIES = new PacketType(SENDER, "Abilities", 57);
				TAB_COMPLETE = new PacketType(SENDER, "TabComplete", 58);
				SCOREBOARD_OBJECTIVE = new PacketType(SENDER, "ScoreboardObjective", 59);
				SCOREBOARD_SCORE = new PacketType(SENDER, "ScoreboardScore", 60);
				SCOREBOARD_DISPLAY_OBJECTIVE = new PacketType(SENDER, "ScoreboardDisplayObjective", 61);
				SCOREBOARD_TEAM = new PacketType(SENDER, "ScoreboardTeam", 62);
				CUSTOM_PAYLOAD = new PacketType(SENDER, "CustomPayload", 63);
				KICK_DISCONNECT = new PacketType(SENDER, "KickDisconnect", 64);
				SERVER_DIFFICULTY = new PacketType(SENDER, "ServerDifficulty", 65);
				COMBAT_EVENT = new PacketType(SENDER, "CombatEvent", 66);
				CAMERA = new PacketType(SENDER, "Camera", 67);
				WORLD_BORDER = new PacketType(SENDER, "WorldBorder", 68);
				TITLE = new PacketType(SENDER, "Title", 69);
				SET_COMPRESSION = new PacketType(SENDER, "SetCompression", 70);
				PLAYER_LIST_HEADER_FOOTER = new PacketType(SENDER, "PlayerListHeaderFooter", 71);
				RESOURCE_PACK_SEND = new PacketType(SENDER, "ResourcePackSend", 72);
				UPDATE_ENTITY_NBT = new PacketType(SENDER, "UpdateEntityNBT", 73);

			}

		}

		public static class Client {

			private static final PacketType.Sender SENDER;
			public static final PacketType KEEP_ALIVE;
			public static final PacketType CHAT;
			public static final PacketType USE_ENTITY;
			public static final PacketType FLYING;
			public static final PacketType POSITION;
			public static final PacketType LOOK;
			public static final PacketType POSITION_LOOK;
			public static final PacketType BLOCK_DIG;
			public static final PacketType BLOCK_PLACE;
			public static final PacketType HELD_ITEM_SLOT;
			public static final PacketType ARM_ANIMATION;
			public static final PacketType ENTITY_ACTION;
			public static final PacketType STEER_VEHICLE;
			public static final PacketType CLOSE_WINDOW;
			public static final PacketType WINDOW_CLICK;
			public static final PacketType TRANSACTION;
			public static final PacketType SET_CREATIVE_SLOT;
			public static final PacketType ENCHANT_ITEM;
			public static final PacketType UPDATE_SIGN;
			public static final PacketType ABILITIES;
			public static final PacketType TAB_COMPLETE;
			public static final PacketType SETTINGS;
			public static final PacketType CLIENT_COMMAND;
			public static final PacketType CUSTOM_PAYLOAD;
			public static final PacketType SPECTATE;
			public static final PacketType RESOURCE_PACK_STATUS;

			static{
				SENDER = Sender.CLIENT;
				KEEP_ALIVE = new PacketType(SENDER, "KeepAlive", 0);
				CHAT = new PacketType(SENDER, "Chat", 1);
				USE_ENTITY = new PacketType(SENDER, "UseEntity", 2);
				FLYING = new PacketType(SENDER, "Flying", 3);
				POSITION = new PacketType(SENDER, "Position", 4);
				LOOK = new PacketType(SENDER, "Look", 5);
				POSITION_LOOK = new PacketType(SENDER, "PositionLook", 6);
				BLOCK_DIG = new PacketType(SENDER, "BlockDig", 7);
				BLOCK_PLACE = new PacketType(SENDER, "BlockPlace", 8);
				HELD_ITEM_SLOT = new PacketType(SENDER, "HeldItemSlot", 9);
				ARM_ANIMATION = new PacketType(SENDER, "ArmAnimation", 10);
				ENTITY_ACTION = new PacketType(SENDER, "EntityAction", 11);
				STEER_VEHICLE = new PacketType(SENDER, "SteerVehicle", 12);
				CLOSE_WINDOW = new PacketType(SENDER, "CloseWindow", 13);
				WINDOW_CLICK = new PacketType(SENDER, "WindowClick", 14);
				TRANSACTION = new PacketType(SENDER, "Transaction", 15);
				SET_CREATIVE_SLOT = new PacketType(SENDER, "SetCreativeSlot", 16);
				ENCHANT_ITEM = new PacketType(SENDER, "EnchantItem", 17);
				UPDATE_SIGN = new PacketType(SENDER, "UpdateSign", 18);
				ABILITIES = new PacketType(SENDER, "Abilities", 19);
				TAB_COMPLETE = new PacketType(SENDER, "TabComplete", 20);
				SETTINGS = new PacketType(SENDER, "Settings", 21);
				CLIENT_COMMAND = new PacketType(SENDER, "ClientCommand", 22);
				CUSTOM_PAYLOAD = new PacketType(SENDER, "CustomPayload", 23);
				SPECTATE = new PacketType(SENDER, "Spectate", 24);
				RESOURCE_PACK_STATUS = new PacketType(SENDER, "ResourcePackStatus", 25);
			}

		}


	}

	public enum Sender {

		SERVER,
		CLIENT

	}

}
